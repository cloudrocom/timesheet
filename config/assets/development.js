'use strict';

module.exports = {
  client: {
    lib: {
      css: [
        'public/lib/font-awesome/css/font-awesome.css',
        'public/lib/angular-material-calendar/dist/angular-material-calendar.css',
        'public/lib/angular-material/angular-material.css',
        'public/lib/angular-aria/angular-aria.css',
        'public/lib/angular-material-data-table/dist/md-data-table.min.css',
        'public/lib/mdPickers/dist/mdPickers.css'
      ],
      js: [
        'public/lib/jquery/dist/jquery.js',

        'public/lib/angular/angular.js',
        'public/lib/angular-resource/angular-resource.js',

        'public/lib/angular-animate/angular-animate.js',
        'public/lib/angular-aria/angular-aria.js',
        'public/lib/angular-route/angular-route.js',

        'public/lib/angular-material/angular-material.js',
        'public/lib/angular-messages/angular-messages.js',

        'public/lib/angular-ui-utils/ui-utils-ieshiv.js',
        'public/lib/angular-ui-utils/ui-utils.js',

        'public/lib/angular-ui-router/release/angular-ui-router.js',

        'public/lib/angular-file-upload/angular-file-upload.js',
        'public/lib/owasp-password-strength-test/owasp-password-strength-test.js',

        'public/lib/angular-capitalize-filter/capitalize.js',

        'public/lib/lodash/lodash.js',
        'public/lib/moment/moment.js',

        'public/lib/angular-moment/angular-moment.js',
        'public/lib/angular-sanitize/angular-sanitize.js',

        'public/lib/angular-material-calendar/dist/angular-material-calendar.js',
        'public/lib/angular-material-data-table/dist/md-data-table.min.js',

        'public/lib/angular-media-queries/match-media.js',
        'public/lib/mdPickers/dist/mdPickers.js',
        'public/lib/ui-scroll-master/src/ui-scroll.js',
        'public/lib/ui-scroll-master/src/ui-scroll-grid.js',
        'public/lib/ui-scroll-master/src/ui-scroll-jqlite.js'

      ],
      tests: ['public/lib/angular-mocks/angular-mocks.js']
    },
    css: [
      'modules/*/client/css/*.css'
    ],
    less: [
      'modules/*/client/less/*.less',
      'public/lib/datetimeRangePicker/range-picker.less'
    ],
    sass: [
      'modules/*/client/scss/*.scss',
      'public/lib/datetimeRangePicker/range-picker.scss'
    ],
    js: [
      'modules/core/client/app/config.js',
      'modules/core/client/app/init.js',
      'modules/*/client/*.js',
      'modules/*/client/**/*.js'
    ],
    img: [
      'modules/**/*/img/**/*.jpg',
      'modules/**/*/img/**/*.png',
      'modules/**/*/img/**/*.gif',
      'modules/**/*/img/**/*.svg'
    ],
    views: ['modules/*/client/views/**/*.html'],
    templates: ['build/templates.js']
  },
  server: {
    gruntConfig: ['gruntfile.js'],
    gulpConfig: ['gulpfile.js'],
    allJS: ['server.js', 'config/**/*.js', 'modules/*/server/**/*.js'],
    models: 'modules/*/server/models/**/*.js',
    routes: ['modules/!(core)/server/routes/**/*.js', 'modules/core/server/routes/**/*.js'],
    sockets: 'modules/*/server/sockets/**/*.js',
    config: ['modules/*/server/config/*.js'],
    policies: 'modules/*/server/policies/*.js',
    views: ['modules/*/server/views/*.html'],
    cronJobs: ['modules/*/server/cronJobs/**/*.js']
  }
};
