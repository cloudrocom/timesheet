'use strict';

var timesheetsPolicy = require('../policies/timesheet.server.policy'),
  timesheets = require('../controllers/timesheet.server.controller'),
  freeDays = require('../controllers/freeDays.server.controller');

module.exports = function(app) {

  /* ----- Timesheet Routes  ----- */

  app.route('/api/timesheets').all(timesheetsPolicy.isAllowed)
    .get(timesheets.list)
    .post(timesheets.create);

  app.route('/api/timesheets/:id').all(timesheetsPolicy.isAllowed)
      .get(timesheets.read)
      .put(timesheets.update)
      .delete(timesheets.delete);

  app.route('/api/timesheets/FindAllByUserId/:userId').all(timesheetsPolicy.isAllowed)
      .get(timesheets.FindAllByUserId);

  app.route('/api/timesheets/FindAllByDate/:userId/:year/:month').all(timesheetsPolicy.isAllowed)
      .get(timesheets.FindAllByDate);

  app.route('/api/timesheets/queue/:userId/:limit/:offset').all(timesheetsPolicy.isAllowed)
      .get(timesheets.queue);

  /* ----- Free Days Routes ----- */

  app.route('/api/freeDays').all(timesheetsPolicy.isAllowed)
    .get(freeDays.list)
    .post(freeDays.create);

  app.route('/api/freeDays/:id').all(timesheetsPolicy.isAllowed)
    .get(freeDays.read)
    .put(freeDays.update)
    .delete(freeDays.delete);

  app.route('/api/freeDays/listByYear/:year/:month').all(timesheetsPolicy.isAllowed)
    .get(freeDays.listByYear);

};
