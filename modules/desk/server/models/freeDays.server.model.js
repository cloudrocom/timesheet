'use strict';

module.exports = function (sequelize, DataTypes) {

  var FreeDays = sequelize.define('FreeDays', {
    date: DataTypes.DATEONLY,
    description: DataTypes.TEXT
  }, {});

  return FreeDays;
};
