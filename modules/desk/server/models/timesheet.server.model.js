'use strict';

module.exports = function (sequelize, DataTypes) {

  var Timesheets = sequelize.define('Timesheets', {

    date: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },

    dayType: DataTypes.TEXT,
    fromHours: DataTypes.TIME,
    toHours: DataTypes.TIME,
    accepted: DataTypes.BOOLEAN,
    requiresEdit: DataTypes.BOOLEAN,
    task: DataTypes.TEXT

  }, {
    classMethods: {
      associate: function (models) {

        Timesheets.belongsTo(models.User, {
          foreignKey: 'UserId',
          foreignKeyConstraint: true
        });

/*        Timesheets.belongsToMany(models.User, {
          through: 'UserTsEntries'
        });*/

        Timesheets.belongsTo(models.Projects, {
          foreignKey: 'ProjectId',
          foreignKeyConstraint: true
        });

      }
    }
  });

  return Timesheets;
};
