'use strict';

/**
 * Module dependencies
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Articles Permissions
 */
exports.invokeRolesPolicies = function () {
  acl.allow([
    {
      roles: ['admin'],
      allows: [
        {
          resources: '/api/timesheets',
          permissions: '*'
        },
        {
          resources: '/api/timesheets/:id',
          permissions: '*'
        },
        {
          resources: '/api/timesheets/FindAllByUserId/:userId',
          permissions: '*'
        },
        {
          resources: '/api/timesheets/FindAllByDate/:userId/:year/:month',
          permissions: '*'
        },
        {
          resources: '/api/timesheets/queue/:userId/:limit/:offset',
          permissions: '*'
        },
        {
          resources: '/api/freeDays',
          permissions: '*'
        },
        {
          resources: '/api/freeDays/:id',
          permissions: '*'
        },
        {
          resources: '/api/freeDays/listByYear/:year/:month',
          permissions: '*'
        }
      ]
    },
    {
      roles: ['user'],
      allows: [
        {
          resources: '/api/timesheets',
          permissions: ['get','post']
        },
        {
          resources: '/api/timesheets/:id',
          permissions: ['get','post', 'put']
        },
        {
          resources: '/api/timesheets/FindAllByUserId/:userId',
          permissions: ['get','post']
        },
        {
          resources: '/api/timesheets/FindAllByDate/:userId/:year/:month',
          permissions: ['get','post']
        },
        {
          resources: '/api/timesheets/queue/:userId/:limit/:offset',
          permissions: ['get','post']
        },
        {
          resources: '/api/freeDays',
          permissions: ['get','post']
        },
        {
          resources: '/api/freeDays/:id',
          permissions: ['get','post', 'put']
        },
        {
          resources: '/api/freeDays/listByYear/:year/:month',
          permissions: ['get']
        }
      ]
    }
  ]);
};

/**
 * Check If Articles Policy Allows
 */
exports.isAllowed = function (req, res, next) {

  var roles = (req.user) ? req.user.dataValues.roles : ['guest'];

  // Check for user roles
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
    if (err) {
      // An authorization error occurred
      return res.status(500).send('Unexpected authorization error');
    } else {
      if (isAllowed) {
        // Access granted! Invoke next middleware
        return next();
      } else {
        return res.status(403).json({
          message: 'User is not authorized'
        });
      }
    }
  });

};