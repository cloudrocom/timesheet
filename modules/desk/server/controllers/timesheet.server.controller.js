'use strict';

var path = require('path'),
  db = require(path.resolve('./config/lib/sequelize')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

function addDays(date, days) {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}

Date.prototype.getWeekNumber = function(){

  var d = new Date(+this);
  d.setHours(0,0,0,0);
  d.setDate(d.getDate()+4-(d.getDay()||7));

  return Math.ceil((((d-new Date(d.getFullYear(),0,1))/8.64e7)+1)/7);

};

exports.create = function (req, res) {

  var fromHours = new Date(req.body.fromHours);
  var toHours = new Date(req.body.toHours);

  db.Timesheets.create({

    UserId: req.body.userId,
    ProjectId: req.body.projectId,

    date: addDays(req.body.date,1),
    dayType: req.body.dayType,

    fromHours: fromHours.getHours() + ':' + fromHours.getMinutes() + ':' + fromHours.getSeconds(),
    toHours: toHours.getHours() + ':' + toHours.getMinutes() + ':' + toHours.getSeconds(),

    task: req.body.task,

    requiresEdit: false,
    accepted: false

  }).then(function (newTimesheet) {

    var user = req.user;
    user.pendingTs = true;

    user
      .save()
      .then(function(user) {
        return res.json(newTimesheet);
      })
      .catch(function(err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

  })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });

    });
};

exports.read = function (req, res) {

  db.Timesheets.find({
    where: {
      id: req.params.id
    }
  })
    .then(function (timesheet) {
      return res.json(timesheet);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.FindAllByUserId = function (req, res) {

  db.Timesheets.findAll({
    where: {
      UserId: parseInt(req.params.userId)
    }
  })
    .then(function (timesheets) {
      return res.json(timesheets);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.FindAllByDate = function (req, res) {

  var month = parseInt(req.params.month);

  var startMonth = '';
  var endMonth = '';

  var startDay = '';
  var endDay = '';

  if(month === 1){
    startMonth = '1';
    endMonth = '2';
    startDay = '1';
    endDay = '31';
  }
  else if(month === 12){

    startMonth = '11';
    endMonth = '12';
    startDay = '1';
    endDay = '31';
  }
  else{
    startMonth = (month - 1).toString();
    endMonth = (month + 1).toString();
    startDay = '20';
    endDay = '10';
  }

  startMonth = startMonth.length === 1 ? '0' + startMonth : startMonth;
  endMonth = endMonth.length === 1 ? '0' + endMonth : endMonth;

  db.Timesheets.findAll({
    where: {
      UserId: parseInt(req.params.userId),
      $and: [
        {
          date: {
            gte: new Date(parseInt(req.params.year) + '-' + startMonth + '-' + startDay)
          }
        },
        {
          date: {
            lte: new Date(parseInt(req.params.year) + '-' + endMonth + '-' + endDay)
          }
        }
      ]
    }
  })
    .then(function (timesheets) {
      return res.json(timesheets);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });
};

exports.update = function (req, res) {

  var fromHours = new Date(req.body.fromHours);
  var toHours = new Date(req.body.toHours);

  db.Timesheets
    .findOne({
      where: {
        id: req.params.id
      }
    })
    .then(function (timesheet) {
      timesheet.updateAttributes({

        //date: addDays(req.body.date,1),
        dayType: req.body.dayType,
        date: req.body.date,
        fromHours: fromHours.getHours() + ':' + fromHours.getMinutes() + ':' + fromHours.getSeconds(),
        toHours: toHours.getHours() + ':' + toHours.getMinutes() + ':' + toHours.getSeconds(),
        accepted: req.body.accepted,
        requiresEdit: req.body.requiresEdit,
        updatedAt: 'NOW()',
        ProjectId: req.body.ProjectId,
        task: req.body.task

      })
        .then(function () {
          return res.json(timesheet);
        })
        .catch(function (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });

      return null;
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.delete = function (req, res) {

  db.Timesheets
    .findOne({
      where: {
        id: req.params.id
      }
    })
    .then(function (timesheet) {
      timesheet.destroy()
        .then(function () {
          return res.json(timesheet);
        })
        .catch(function (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });

      return null;
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.list = function (req, res) {

  db.Timesheets.findAll({
    include: [
      db.User
    ]
  })
    .then(function (timesheets) {
      return res.json(timesheets);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.queue = function (req, res) {

  var limit = req.params.limit;
  var offset = req.params.offset;

  //limit = 10;
  //offset = 0;

  db.User.findAndCountAll({

    where: {
      pendingTs: true
    },
    attributes: ['id'],
    limit: limit,
    offset: offset,
    order: [
      ['displayName', 'DESC']
    ]

  })
    .then(function (users) {

      var userIds = [];

      for(var key in users.rows){
        userIds.push(users.rows[key].dataValues.id);
      }

      db.Timesheets
        .findAndCountAll({
          where: {
            UserId: userIds,
            accepted: false,
            requiresEdit: false
          },
          include: [{
            all: true
          }]
        })
        .then(function (timesheet) {

          var result = JSON.parse(JSON.stringify(timesheet));

          /* --- START --- */

          var memoryObj = {};

          for (var index in result.rows) {

            var user = result.rows[index].User;
            var project = result.rows[index].Project;
            var timesheetEntry = result.rows[index];

            var timesheetEntryDate = timesheetEntry.date.toString();
            var weekNumber = new Date(timesheetEntry.date).getWeekNumber();

            var uuid = user.id + '-' + user.email;

            delete timesheetEntry.User;
            delete timesheetEntry.Project;

            if(memoryObj[uuid] === undefined)
              memoryObj[uuid] = {};

            memoryObj[uuid].user = user;
            //memoryObj[uuid].collapse = false;

            if (memoryObj[uuid].entries === undefined)
              memoryObj[uuid].entries = {};

            if (memoryObj[uuid].entries[weekNumber.toString()] === undefined)
              memoryObj[uuid].entries[weekNumber.toString()] = {};

            memoryObj[uuid].entries[weekNumber.toString()].collapse = false;

            if (memoryObj[uuid].entries[weekNumber.toString()][timesheetEntryDate] === undefined)
              memoryObj[uuid].entries[weekNumber.toString()][timesheetEntryDate] = {};

            memoryObj[uuid].entries[weekNumber.toString()][timesheetEntryDate].collapse = false;

            if (memoryObj[uuid].entries[weekNumber.toString()][timesheetEntryDate].data === undefined)
              memoryObj[uuid].entries[weekNumber.toString()][timesheetEntryDate].data = [];

            memoryObj[uuid].entries[weekNumber.toString()][timesheetEntryDate].data.push({
              entry: timesheetEntry,
              project: project
            });

          }

          /* --- END --- */

          var returnObject = [];

          for(var i in memoryObj){
            returnObject.push(memoryObj[i]);
          }

          return res.json(returnObject);
        })
        .catch(function (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });

      //return res.json(users); // async so it won't return users!!

    })
    .catch(function (err) {

      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });

    });

};
