'use strict';

var path = require('path'),
  db = require(path.resolve('./config/lib/sequelize')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

exports.create = function (req, res) {

  db.FreeDays.create({
    date: req.body.date,
    description: req.body.description
  })
    .then(function (newFreeDay) {
      return res.json(newFreeDay);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });
};

exports.read = function (req, res) {

  db.FreeDays.find({
    where: {
      id: req.params.id
    }
  })
    .then(function (freeDay) {
      return res.json(freeDay);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.update = function (req, res) {

  db.FreeDays
    .findOne({
      where: {
        id: req.params.id
      }
    })
    .then(function (freeDay) {
      freeDay.updateAttributes({

        date: req.body.date,
        description: req.body.description

      })
        .then(function () {
          return res.json(freeDay);
        })
        .catch(function (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });

      return null;
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.delete = function (req, res) {

  db.FreeDays
    .findOne({
      where: {
        id: req.params.id
      }
    })
    .then(function (freeDay) {
      freeDay.destroy()
        .then(function () {
          return res.json(freeDay);
        })
        .catch(function (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });

      return null;
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.list = function (req, res) {

  db.FreeDays.findAll({})
    .then(function (freeDays) {
      return res.json(freeDays);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.listByYear = function (req, res) {

  var month = parseInt(req.params.month);

  var startMonth = '';
  var endMonth = '';

  var startDay = '';
  var endDay = '';

  if(month === 1){
    startMonth = '1';
    endMonth = '2';
    startDay = '1';
    endDay = '31';
  }
  else if(month === 12){

    startMonth = '11';
    endMonth = '12';
    startDay = '1';
    endDay = '31';
  }
  else{
    startMonth = (month - 1).toString();
    endMonth = (month + 1).toString();
    startDay = '20';
    endDay = '10';
  }

  startMonth = startMonth.length === 1 ? '0' + startMonth : startMonth;
  endMonth = endMonth.length === 1 ? '0' + endMonth : endMonth;

  db.FreeDays.findAll({
    where: {
      date: {
        gte: new Date(parseInt(req.params.year) + '-' + startMonth + '-' + startDay),
        lte: new Date(parseInt(req.params.year) + '-' + endMonth + '-' + endDay)
      }
    }
  })
    .then(function (freeDays) {
      return res.json(freeDays);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};
