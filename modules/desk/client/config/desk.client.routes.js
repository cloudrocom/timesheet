(function () {
  'use strict';

  angular
      .module('desk.routes')
      .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
        .state('desk', {
          abstract: true,
          url: '/desk',
          template: '<ui-view/>'
        })
        .state('desk.timesheet', {
          url: '/timesheet',
          templateUrl: 'modules/desk/client/views/desk.timesheet.client.view.html',
          controller: 'DeskTimesheetController',
          controllerAs: 'vm'
        });
  }

})();