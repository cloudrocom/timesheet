(function () {
  'use strict';

  angular.module('desk.services').factory('TimesheetsService', TimesheetsService);

  TimesheetsService.$inject = ['$resource'];

  function TimesheetsService($resource) {

    var obj = {};

    obj.workDayTypes = ['Overtime','Normal','Weekend','Holiday','Travel', 'Stand-By'];

    obj.api = $resource('api/timesheets/:id', {
      id: '@id'
    }, {
      update: {
        method: 'PUT'
      },
      FindAllByUserId: {
        url: 'api/timesheets/FindAllByUserId/:userId',
        method: 'GET',
        isArray: true
      },
      FindAllByDate: {
        url: 'api/timesheets/FindAllByDate/:userId/:year/:month',
        method: 'GET',
        isArray: true
      },
      queue: {
        url: 'api/timesheets/queue/:userId/:limit/:offset',
        method: 'GET',
        isArray: true
      }
    });

    return obj;
  }

})();