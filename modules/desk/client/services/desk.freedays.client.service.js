(function () {
  'use strict';

  angular.module('desk.services').factory('FreeDaysService', FreeDaysService);

  FreeDaysService.$inject = ['$resource'];

  function FreeDaysService($resource) {

    return $resource('api/freeDays', {}, {
      update: {
        method: 'PUT'
      },
      create: {
        method: 'POST'
      },
      delete: {
        method: 'DELETE'
      },
      listByYear: {
        url: 'api/freeDays/listByYear/:year/:month',
        method: 'GET',
        isArray: true
      }
    });

  }

})();