(function (app) {
  'use strict';

  app.registerModule('desk');
  app.registerModule('desk.services');
  app.registerModule('desk.routes', ['ui.router', 'desk.services']);
})(ApplicationConfiguration);
