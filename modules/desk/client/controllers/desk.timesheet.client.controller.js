'use strict';
/*jshint loopfunc: true */
angular.module('desk').controller('DeskTimesheetController',
  ['$scope', '$filter', '$http', '$q', 'Users',
    'GlobalService', 'TimesheetsService', 'HolidaysService', 'ClientsService', 'ProjectsService', 'FreeDaysService',
    'Authentication', 'MaterialCalendarData', '$mdToast', 'ToastService', 'screenSize', '$mdpTimePicker', 'moment',
    '$window',

    function ($scope, $filter, $http, $q, Users,
              GlobalService, TimesheetsService, HolidaysService, ClientsService, ProjectsService, FreeDaysService,
              Authentication, MaterialCalendarData, $mdToast, ToastService, screenSize, $mdpTimePicker, moment,
              $window) {

      var vm = this;
      vm.authentication = Authentication;

      function isMobile(){
        return screenSize.is('xs, sm');
      }

      $scope.calendarFlexValue = isMobile() ? 95 : 65;
      $scope.rowOrColumnForMobile = isMobile() ? 'column' : 'row';
      $scope.verticalTimeDividerCss = isMobile() ? '' : 'border-left: 1px; border-left-style: solid; border-left-color: rgba(0,0,0,0.12)';
      //$scope.fabIcons = isMobile() ? 'width: 35px;height: 24px' : 'width: 35px;height: 24px';
      $scope.whiteFrameForActions = isMobile() ? '' : 'md-whiteframe-1dp';
      $scope.elementsWidth = isMobile() ? 'justify-content: space-around; width:100%' : 'justify-content: space-around;';

      var w = angular.element($window);
      $scope.$watch(
        function () {
          return $window.innerWidth;
        },
        function (value) {
          responsiveHelper();
          $scope.windowWidth = value;
        },
        true
      );

      w.bind('resize', function(){
        $scope.$apply();
      });

      var user = null; // will hold user object data, not seen in the request to the timesheets
      var timesheetEntryData = [];

      var currentYear = null;
      var currentMonth = null;

      initialize(); // runs the init function to fetch current month data

      $scope.selectedDates = []; // Current selected dates by user

      //User input used on create a new timesheet entry
      $scope.project = null;
      $scope.userProjects = null;

      $scope.workDayTypes = TimesheetsService.workDayTypes;
      $scope.currentWorkDayType = null;

      $scope.readOnly = false;
      $scope.removeAble = true;

      $scope.task = null;

      $scope.disabled = ['this','that']; // TODO try this in calendar directive/sc ??

      var weekday = GlobalService.weekDays;

      //Edit related data
      $scope.currentSelectedObjectForEdit = undefined;
      $scope.currentSelectedObjectsForEditIds = [];

      $scope.timesheetObjects = null;
      $scope.freeDaysObjects = null;
      $scope.holidaysObjects = null;

      var loopPromises = [];

      var defaultStartHours = ('11-11-2011 08:00:00 AM').replace(/-/g,'/');
      var defaultEndHours = ('11-11-2011 06:00:00 PM').replace(/-/g,'/');

      $scope.timesheetEntryData = [
        {
          id: null,
          entryId: 0,
          startTime: new Date(defaultStartHours),
          endTime: new Date(defaultEndHours),
          currentWorkDayType: null,
          project: null,
          task: null,
          date: null
        }
      ];

      $scope.displaySelectedDates = [];

      /* --- CALENDAR RELATED --- */

      $scope.dayClick = function (date) {

        var exists = false;

        for (var key in timesheetEntryData) {

          if (new Date(key).sameDay(date)) {
            exists = true;

            var aNew = typeof $scope.currentSelectedObjectForEdit === 'undefined';

            if(!aNew) {

              for (var index1 in $scope.currentSelectedObjectForEdit) {

                for (var index2 in timesheetEntryData[key]) {

                  aNew = ($scope.currentSelectedObjectForEdit[index1].ProjectId !== timesheetEntryData[key][index2].ProjectId ||
                    $scope.currentSelectedObjectForEdit[index1].dayType !== timesheetEntryData[key][index2].dayType ||
                    $scope.currentSelectedObjectForEdit[index1].fromHours !== timesheetEntryData[key][index2].fromHours ||
                    $scope.currentSelectedObjectForEdit[index1].toHours !== timesheetEntryData[key][index2].toHours ||
                    $scope.currentSelectedObjectForEdit[index1].task !== timesheetEntryData[key][index2].task);

                }
              }
            }

            if (aNew) {

              $scope.currentSelectedObjectForEdit = timesheetEntryData[key];
              $scope.timesheetEntryData = [];

              var myInt = 0;
              for (var index in timesheetEntryData[key]) {

                var currentProject = null;
                for (var val in $scope.userProjects) {
                  if($scope.userProjects[val].id === timesheetEntryData[key][index].ProjectId) {
                    currentProject = $scope.userProjects[val];
                  }
                }

                $scope.timesheetEntryData.push({
                  id: timesheetEntryData[key][index].id,
                  entryId: myInt,
                  startTime: new Date('11-11-2011-' + timesheetEntryData[key][index].fromHours),
                  endTime: new Date('11-11-2011-' + timesheetEntryData[key][index].toHours),
                  currentWorkDayType: timesheetEntryData[key][index].dayType,
                  project: currentProject,
                  task: timesheetEntryData[key][index].task,
                  date: timesheetEntryData[key][index].date
                });

                myInt++;
              }

              $scope.currentSelectedObjectsForEditIds = [];

              $scope.currentSelectedObjectsForEditIds.push({
                elements: timesheetEntryData[key],
                date: date
              });

            }
            else {

              $scope.currentSelectedObjectsForEditIds.push({
                elements: timesheetEntryData[key],
                date: date
              });

            }

            break;
          }

        }

        if (exists) {
          // EDIT CURR TS

          $scope.create = false;
          $scope.edit = true;

          if ($scope.currentSelectedObjectsForEditIds.length === 1) {
            $scope.selectedDates = [date];
            if(!screenSize.is('xs, sm'))
              GlobalService.anchorScroll('createOrDelete');
          }

        }
        else {
          // CREATE NEW TS

          for(var freeDaysIndex in $scope.freeDaysObjects){

            if ((new Date($scope.freeDaysObjects[freeDaysIndex].date)).sameDay(date)) {

              $scope.create = false;
              $scope.edit = false;
              $scope.selectedDates = [];
              clearFields();

              return 0;
            }
          }

          for(var holidaysIndex in $scope.holidaysObjects){
            for (var i = 0; i < GlobalService.dateDiffInDays($scope.holidaysObjects[holidaysIndex].fromDate,
              $scope.holidaysObjects[holidaysIndex].toDate) + 1; i++) {

              if (GlobalService.addDays($scope.holidaysObjects[holidaysIndex].fromDate, i).sameDay(date)) {

                $scope.create = false;
                $scope.edit = false;
                $scope.selectedDates = [];
                clearFields();

                return 0;
              }
            }
          }

          $scope.create = true;
          $scope.edit = false;

          if (typeof $scope.currentSelectedObjectForEdit !== 'undefined') {

            $scope.currentSelectedObjectForEdit = undefined;
            $scope.currentSelectedObjectsForEditIds = [];
            $scope.selectedDates = [date];

            clearFields();
            initTimesheetDataArray();

            if(!screenSize.is('xs, sm'))
              GlobalService.anchorScroll('createOrDelete');

          }

        }

        if($scope.selectedDates.length <= 0){
          $scope.create = false;
          $scope.edit = false;
          initTimesheetDataArray();
        }

        $scope.displaySelectedDates = [];
        for(var dateIndex in $scope.selectedDates){

          var datey = new Date($scope.selectedDates[dateIndex]);

          $scope.displaySelectedDates.push({
            dayName: weekday[datey.getDay()],
            dayNumber: datey.getDate(),
            month: datey.getMonth() + 1,
            year: datey.getFullYear()
          });

        }

        $scope.displaySelectedDates = $scope.displaySelectedDates.sort(function(a,b){
          return a.dayNumber - b.dayNumber;
        });

      };

      $scope.prevMonth = function (data) {
        $scope.displaySelectedDates = [];
        $scope.selectedDates = []; // Clear the current selected dates

        $scope.edit = false;
        $scope.create = false;

        fetchMonth(data);// Gets the data for the previous month
      };

      $scope.nextMonth = function (data) {
        $scope.displaySelectedDates = [];
        $scope.selectedDates = []; // Clear the current selected dates

        $scope.edit = false;
        $scope.create = false;

        fetchMonth(data); // Gets the data for the next month
      };

      $scope.setDayContent = function (date) {
        return '<p></p>';
      };

      $scope.setDirection = function (direction) {
        //Calendar specific configuration
        $scope.direction = direction;
        $scope.dayFormat = direction === 'vertical' ? 'EEEE, MMMM d' : 'd';
      };

      /* --- FUNCTIONS --- */

      $scope.insertTimesheetData = function () {

        loopPromises = [];
        var deferred = $q.defer();

        for (var i in $scope.selectedDates) {

          for(var entry in $scope.timesheetEntryData){

            var sTime = $scope.timesheetEntryData[entry].startTime;
            var eTime = $scope.timesheetEntryData[entry].endTime;

            loopPromises.push($http.post('/api/timesheets', {

              date: $scope.selectedDates[i],
              dayType: $scope.timesheetEntryData[entry].currentWorkDayType,
              fromHours: sTime,
              toHours: eTime,
              userId: user.id,
              projectId: $scope.timesheetEntryData[entry].project.id,
              task: $scope.timesheetEntryData[entry].task

            }));

            /*            $http.post('/api/timesheets', {

             date: $scope.selectedDates[i],
             dayType: $scope.timesheetEntryData[entry].currentWorkDayType,
             fromHours: sTime,
             toHours: eTime,
             userId: user.id,
             projectId: $scope.timesheetEntryData[entry].project.id,
             task: $scope.timesheetEntryData[entry].task

             }).success(function (response) {
             // If successful we assign the response to the calendar
             //MaterialCalendarData.setDayContent(new Date(response.date), '<font color=\"red\">' + response.dayType + '</font>');

             ToastService.show('Entry inserted!',1000);

             }).error(function (response) {
             $scope.error = response.message;
             ToastService.show('Entry not inserted!',1000);
             });*/

          }

        }

        $scope.selectedDates = []; // clear the selected dates
        $scope.create = false;

        clearFields();
        initTimesheetDataArray();
        //fetchTimesheets(GlobalService.currentDate.year(), GlobalService.currentDate.month());
        GlobalService.anchorScroll('calendarDiv');

        $q.all(loopPromises).then(function (data) {

          fetchTimesheets(currentYear, currentMonth);
          ToastService.show('Entry inserted!', 1000);

          //deferred.resolve(data);
        });

      };

      $scope.updateTimesheetData = function () {

        loopPromises = [];
        var deferred = $q.defer();

        for (var y in $scope.currentSelectedObjectsForEditIds) { // for each day selected to be edited

          for (var x in $scope.currentSelectedObjectsForEditIds[y].elements) { // for each entry of the current day

            loopPromises.push($http.put('/api/timesheets/' + $scope.currentSelectedObjectsForEditIds[y].elements[x].id, {

              date: $scope.timesheetEntryData[x].date,
              dayType: $scope.timesheetEntryData[x].currentWorkDayType,
              fromHours: $scope.timesheetEntryData[x].startTime,
              toHours: $scope.timesheetEntryData[x].endTime,
              task: $scope.timesheetEntryData[x].task,
              updatedAt: 'NOW()',
              projectId: $scope.timesheetEntryData[x].project.id

            }));

            /*
             $http.put('/api/timesheets/' + $scope.currentSelectedObjectsForEditIds[y].elements[x].id, {

             date: $scope.timesheetEntryData[x].date,
             dayType: $scope.timesheetEntryData[x].currentWorkDayType,
             fromHours: $scope.timesheetEntryData[x].startTime,
             toHours: $scope.timesheetEntryData[x].endTime,
             task: $scope.timesheetEntryData[x].task,
             updatedAt: 'NOW()',
             projectId: $scope.timesheetEntryData[x].project.id

             }).success(function (response) {
             // If successful we assign the response to the calendar
             //MaterialCalendarData.setDayContent(new Date(response.date), '<font color=\"red\">' + response.dayType + '</font>');

             deferred.resolve();
             ToastService.show('Entry updated!', 1000);

             }).error(function (response) {
             $scope.error = response.message;
             ToastService.show('Entry not updated!', 1000);
             });
             */

          }
        }

        $scope.selectedDates = []; // clear the selected dates

        $scope.edit = false;

        $scope.currentSelectedObjectForEdit = undefined;
        $scope.currentSelectedObjectsForEditIds = [];
        $scope.selectedDates = [];

        initTimesheetDataArray();

        clearFields();
        GlobalService.anchorScroll('calendarDiv');

        $q.all(loopPromises).then(function (data) {

          fetchTimesheets(currentYear, currentMonth);
          ToastService.show('Entry updated!', 1000);

          //deferred.resolve(data);
        });

      };

      $scope.removeTimesheetEntryElement = function(entryId){

        for (var key in $scope.timesheetEntryData) {
          if($scope.timesheetEntryData[key].entryId === entryId){
            $scope.timesheetEntryData.splice(key, 1);
          }
        }

        for (var i = 0; i < $scope.timesheetEntryData.length; i++) {
          $scope.timesheetEntryData[i].entryId = i;
        }

      };

      $scope.addNewTimesheetDataEntry = function(){

        var entryId = $scope.timesheetEntryData.length;

        $scope.timesheetEntryData.push({
          entryId: entryId,
          startTime: new Date('11-11-2011-8:00'),
          endTime: new Date('11-11-2011-18:00'),
          currentWorkDayType: null,
          project: null,
          task: null
        });

        GlobalService.anchorScroll('entry-' + entryId);
      };

      $scope.cloneNewTimesheetDataEntry = function(entryId){

        var newEntryId = $scope.timesheetEntryData.length;

        for (var key in $scope.timesheetEntryData) {

          if($scope.timesheetEntryData[key].entryId === entryId){

            $scope.timesheetEntryData.push({
              entryId: newEntryId,
              startTime: $scope.timesheetEntryData[key].startTime,
              endTime: $scope.timesheetEntryData[key].endTime,
              currentWorkDayType: $scope.timesheetEntryData[key].currentWorkDayType,
              project: $scope.timesheetEntryData[key].project,
              task: $scope.timesheetEntryData[key].task
            });

          }
        }

        GlobalService.anchorScroll('entry-' + newEntryId);
      };

      function fetchMonth(date) {
        fetchFreeDays(date.year, date.month);
      }

      function fetchTimesheets(year, month){

        TimesheetsService.api.FindAllByDate({

          userId: user.id,
          year: year,
          month: month

        })
          .$promise.then(function (data) {

            // gets the data, takes it out into stringify, transforms it onto an javascript object.
            $scope.timesheetObjects = JSON.parse(JSON.stringify(data));

            //In the foreach, it iterates through each day, used to set <HTML> tags inside the day square
            //For each properties check Server->Models
            timesheetEntryData = [];

            if($scope.timesheetObjects.length !== 0) {

              for (var key in $scope.timesheetObjects) {

                var currObj = $scope.timesheetObjects[key];

                if(!timesheetEntryData[currObj.date]){
                  timesheetEntryData[currObj.date] = [];
                }

                timesheetEntryData[currObj.date].push($scope.timesheetObjects[key]);

                timesheetEntryData[currObj.date] = timesheetEntryData[currObj.date].sort(function(y, z) {
                  var a = new Date('11-11-2011-' + y.fromHours), b = new Date('11-11-2011-' + z.fromHours);
                  return (a.getTime() - b.getTime());
                });

              }

              for (var entryKey in timesheetEntryData) {
                MaterialCalendarData.setDayContent(new Date(entryKey), timesheetEntryData[entryKey][timesheetEntryData[entryKey].length - 1].dayType);
              }

            }
            //fetchFreeDays(date.year, date.month);
            //fetchHolidays(date.year, date.month);

          });

      }

      function fetchFreeDays(year, month){

        currentYear = year;
        currentMonth = month;

        FreeDaysService.listByYear({
          year: year,
          month: month
        })
          .$promise.then(function (datax) {

            $scope.freeDaysObjects = JSON.parse(JSON.stringify(datax));

            for (var dayKey in $scope.freeDaysObjects) {

              MaterialCalendarData.setDayContent(new Date($scope.freeDaysObjects[dayKey].date),
                  '<font color=\"red\">' + $scope.freeDaysObjects[dayKey].description + '</font>'); //$scope.freeDaysObjects[dayKey].description
            }
            fetchHolidays(year, month);
          });

      }

      function fetchHolidays(year, month){

        HolidaysService.FindAllByDate({
          userId: user.id,
          year: year,
          month: month
        }) // Used to populate entire calendar with all the holidays data at first
          .$promise.then(function (data) {
            // gets the data, takes it out into stringify, transforms it onto an javascript object.
            $scope.holidaysObjects = JSON.parse(JSON.stringify(data));
            //In the foreach, it iterates through each day, used to set <HTML> tags inside the day square
            //For each properties check Server->Models
            for (var key in $scope.holidaysObjects) {

              var currentObj = $scope.holidaysObjects[key];

              for (var i = 0; i < GlobalService.dateDiffInDays(currentObj.fromDate, currentObj.toDate) + 1; i++) {
                MaterialCalendarData.setDayContent(GlobalService.addDays(currentObj.fromDate, i), '<font color=\"orange\">' + currentObj.description + '</font>');
              }
            }
            fetchTimesheets(year, month);
          });

      }

      function initialize() {

        user = vm.authentication.user;

        if (user.assignedProjects) {

          ProjectsService.FindAllByIds({
            ids: JSON.stringify(user.assignedProjects)
          }).$promise.then(function (projects) {
            $scope.userProjects = JSON.parse(JSON.stringify(projects));
          });

        }

        //fetch initial data for the current month
        fetchMonth({
          year: GlobalService.currentDate.year(),
          month: GlobalService.currentDate.month()
        });

      }

      function clearFields(){

        $scope.currentWorkDayType = null;
        $scope.startTime = null;
        $scope.endTime = null;
        $scope.task = '';

        $scope.project = null;

      }

      function responsiveHelper(){

        $scope.calendarFlexValue = isMobile() ? 95 : 65;
        $scope.rowOrColumnForMobile = isMobile() ? 'column' : 'row';
        $scope.verticalTimeDividerCss = isMobile() ? '' : 'border-left: 1px; border-left-style: solid; border-left-color: rgba(0,0,0,0.12)';
        //$scope.fabIcons = isMobile() ? 'width: 35px;height: 24px' : 'width: 35px;height: 24px';
        $scope.whiteFrameForActions = isMobile() ? 'flex-100 layout-column' : 'flex-65 layout-row md-whiteframe-1dp';
        $scope.elementsWidth = isMobile() ? 'justify-content: space-around; width:95%' : 'justify-content: space-around;';
      }

      function initTimesheetDataArray(){

        $scope.timesheetEntryData = [
          {
            entryId: 0,
            startTime: new Date('11-11-2011-8:00'),
            endTime: new Date('11-11-2011-18:00'),
            currentWorkDayType: null,
            project: null,
            task: null,
            date: null,
            id: null
          }
        ];

      }

      /* --- TIME PICKERS --- */

      $scope.filterDate = function(date) {
        return moment(date).date() % 2 === 0;
      };

      $scope.showTimePicker = function(ev,type,index) {

        if(type === 'start'){
          $mdpTimePicker($scope.timesheetEntryData[index].startTime, {
            targetEvent: ev
          }).then(function(selectedDate) {
            $scope.timesheetEntryData[index].startTime = selectedDate;
          });
        }
        else if(type === 'end'){
          $mdpTimePicker($scope.timesheetEntryData[index].endTime, {
            targetEvent: ev
          }).then(function(selectedDate) {
            $scope.timesheetEntryData[index].endTime = selectedDate;
          });
        }

      };

    }
  ]);