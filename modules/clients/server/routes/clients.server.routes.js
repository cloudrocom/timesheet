'use strict';

var clientsPolicy = require('../policies/clients.server.policy'),
  clients = require('../controllers/clients.server.controller');

var projectsPolicy = require('../policies/clients.projects.server.policy'),
  projects = require('../controllers/clients.projects.server.controller');

module.exports = function(app) {

  /* --- CLIENTS --- */

  app.route('/api/clients').all(clientsPolicy.isAllowed)
    .get(clients.list)
    .post(clients.create);

  app.route('/api/clients/:id').all(clientsPolicy.isAllowed)
      .get(clients.read)
      .put(clients.update)
      .delete(clients.remove);

  app.route('/api/clients/data/:ids').all(clientsPolicy.isAllowed)
      .get(clients.listAllByIds);

  /* --- PROJECTS --- */

  app.route('/api/projects').all(projectsPolicy.isAllowed)
    .get(projects.list)
    .post(projects.create);

  app.route('/api/projects/:id').all(projectsPolicy.isAllowed)
    .get(projects.read)
    .put(projects.update)
    .delete(projects.remove);

  app.route('/api/projects/FindAllByClientId/:clientId').all(projectsPolicy.isAllowed)
    .get(projects.listAllByClientId);

  app.route('/api/projects/FindAllByIds/:ids').all(projectsPolicy.isAllowed)
    .get(projects.listAllByIds);

};
