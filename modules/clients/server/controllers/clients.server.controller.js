'use strict';

var path = require('path'),
  db = require(path.resolve('./config/lib/sequelize')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

exports.create = function (req, res) {

  db.Clients.create({
    name: req.body.name
  })
      .then(function (newClient) {
        return res.json(newClient);
      })
      .catch(function (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });
};

exports.read = function (req, res) {

  db.Clients.find({
    where: {
      id: req.params.id
    }
  })
      .then(function (client) {
        return res.json(client);
      })
      .catch(function (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

};

exports.update = function (req, res) {

  db.Clients
      .findOne({
        where: {
          id: req.params.clientid
        }
      })
      .then(function (client) {
        client.updateAttributes({
          name: req.body.name
        })
            .then(function () {
              return res.json(client);
            })
            .catch(function (err) {
              return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
              });
            });

        return null;
      })
      .catch(function (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

};

exports.remove = function (req, res) {

  db.Clients
      .findOne({
        where: {
          id: req.params.clientid
        }
      })
      .then(function (client) {
        client.destroy()
            .then(function () {
              return res.json(client);
            })
            .catch(function (err) {
              return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
              });
            });

        return null;
      })
      .catch(function (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

};

exports.list = function (req, res) {

  db.Clients.findAll({})
      .then(function (client) {
        return res.json(client);
      })
      .catch(function (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

};

exports.listAllByIds = function (req, res) {

  console.log('---> ' + req.params.ids + ' <---');

  db.Clients.findAll({
    where: {
      id: JSON.parse(req.params.ids)
    }
  })
      .then(function (client) {
        return res.json(client);
      })
      .catch(function (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

};
