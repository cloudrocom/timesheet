'use strict';

var path = require('path'),
  db = require(path.resolve('./config/lib/sequelize')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

exports.create = function (req, res) {

  db.Projects.create({
    name: req.body.name
  })
    .then(function (newProject) {
      return res.json(newProject);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });
};

exports.read = function (req, res) {

  db.Projects.find({
    where: {
      id: req.params.id
    }/*,
     include: [
     db.User
     ]*/
  })
    .then(function (project) {
      return res.json(project);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.update = function (req, res) {

  db.Project
    .findOne({
      where: {
        id: req.params.projectid
      },
      include: [
        db.User
      ]
    })
    .then(function (project) {
      project.updateAttributes({
        name: req.body.name
      })
        .then(function () {
          return res.json(project);
        })
        .catch(function (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });

      return null;
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.remove = function (req, res) {

  db.Projects
    .findOne({
      where: {
        id: req.params.projectid
      },
      include: [
        db.User
      ]
    })
    .then(function (project) {
      project.destroy()
        .then(function () {
          return res.json(project);
        })
        .catch(function (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });

      return null;
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.listAllByClientId = function (req, res) {

  db.Projects.findAll({
    where: {
      ClientId: parseInt(req.params.clientid)
    }
  })
    .then(function (projects) {
      return res.json(projects);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.list = function (req, res) {

  db.Projects.findAll({
    /*    include: [
     db.User
     ]*/
  })
    .then(function (project) {
      return res.json(project);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.listAllByIds = function (req, res) {

  db.Projects.findAll({
    where: {
      id: JSON.parse(req.params.ids)/*,
      ClientId: parseInt(req.params.clientid)*/
    }
  })
    .then(function (project) {
      return res.json(project);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};
