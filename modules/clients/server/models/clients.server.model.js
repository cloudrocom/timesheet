'use strict';

module.exports = function (sequelize, DataTypes) {

  var Clients = sequelize.define('Clients', {

    name: DataTypes.TEXT,
    contactDate: DataTypes.DATEONLY,

    contracts: DataTypes.JSONB,
    partners: DataTypes.JSONB

  }, {
    classMethods: {
      associate: function (models) {
        Clients.hasMany(models.Projects);
      }
    }
  });

  return Clients;
};
