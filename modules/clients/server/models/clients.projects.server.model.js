'use strict';

module.exports = function (sequelize, DataTypes) {

  var Projects = sequelize.define('Projects', {

    name: DataTypes.TEXT,

    teamLead: DataTypes.JSONB,
    projectManager: DataTypes.JSONB,
    peopleManager: DataTypes.JSONB,

    participants: DataTypes.ARRAY(DataTypes.INTEGER),
    startDate: DataTypes.DATEONLY,
    endDate: DataTypes.DATEONLY

  }, {

    classMethods: {
      associate: function (models) {

        Projects.belongsTo(models.Clients, {
          foreignKey: 'ClientId',
          foreignKeyConstraint: true
        });

      }
    }
  });

  return Projects;
};
