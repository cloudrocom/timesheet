(function () {
  'use strict';

  angular
      .module('clients.services')
      .factory('ClientsService', ClientsService);

  ClientsService.$inject = ['$resource'];

  function ClientsService($resource) {

    return $resource('api/clients/:id', {
      id: '@id'
    }, {
      update: {
        method: 'PUT'
      },
      listAllByIds: {
        url: 'api/clients/data/:ids',
        method: 'GET',
        isArray: true
      }
    });

  }
})();