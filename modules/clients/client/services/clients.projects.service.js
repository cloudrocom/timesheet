(function () {
  'use strict';

  angular
    .module('clients.services')
    .factory('ProjectsService', ProjectsService);

  ProjectsService.$inject = ['$resource'];

  function ProjectsService($resource) {

    return $resource('api/projects/:id', {
      id: '@id'
    }, {
      update: {
        method: 'PUT'
      },
      FindAllByClientId: {
        url: 'api/projects/FindAllByClientId/:clientId',
        method: 'GET',
        isArray: true
      },
      FindAllByIds: {
        url: 'api/projects/FindAllByIds/:ids',
        method: 'GET',
        isArray: true
      }
    });

  }
})();