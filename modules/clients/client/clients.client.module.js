(function (app) {
  'use strict';

  app.registerModule('clients');
  app.registerModule('clients.services');
  app.registerModule('clients.routes', ['ui.router', 'clients.services']);
})(ApplicationConfiguration);
