(function() {
  'use strict';

  angular
    .module('clients.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {

    $stateProvider

      /* --- CLIENTS --- */

      .state('clients', {
        abstract: true,
        url: '/clients',
        template: '<ui-view/>'
      })
      .state('clients.list', {
        url: '/list',
        templateUrl: 'modules/clients/client/views/clients.client.view.html',
        controller: 'ClientsController',
        controllerAs: 'vm'
      })

      /* --- PROJECTS --- */

      .state('projects', {
        abstract: true,
        url: '/projects',
        template: '<ui-view/>'
      })
      .state('projects.list', {
        url: '/list',
        templateUrl: 'modules/clients/client/views/clients.projects.client.view.html',
        controller: 'ProjectsController',
        controllerAs: 'vm'
      });

  }
})();