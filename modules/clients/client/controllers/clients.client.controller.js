(function() {
  'use strict';

  angular
    .module('clients')
    .controller('ClientsController', ClientsController);

  ClientsController.$inject = ['$http', '$scope', '$state', 'Authentication'];

  function ClientsController($http, $scope, $state, Authentication) {

    var vm = this;
    vm.user = Authentication.user;

  }
})();