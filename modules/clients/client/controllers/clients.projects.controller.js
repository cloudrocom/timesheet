(function() {
  'use strict';

  angular
    .module('clients')
    .controller('ProjectsController', ProjectsController);

  ProjectsController.$inject = ['$http', '$scope', '$state', 'Authentication'];

  function ProjectsController($http, $scope, $state, Authentication) {

    var vm = this;
    vm.user = Authentication.user;

  }
})();