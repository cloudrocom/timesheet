'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
  fs = require('fs'),
  path = require('path'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  multer = require('multer'),
  db = require(path.resolve('./config/lib/sequelize')),
  config = require(path.resolve('./config/config'));

/**
 * Update user details
 */

exports.update = function(req, res) {

  if(req.get('Referrer').indexOf('/admin/edit') !== -1){

    db.User
      .findOne({
        where: {
          id: req.body.id
        }
      })
      .then(function(user) {
        updateUser(user,req,res, 'admin');
        return null;
      })
      .catch(function(err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

  }
  else{
    updateUser(req.user,req,res, 'profile');
  }

};

function updateUser(user, req, res, task){

  delete req.body.roles;

  if (user) {

    user = _.extend(user, req.body);
    user.displayName = user.firstName + ' ' + user.lastName;

    user.assignedClients = user.assignedClients ? user.assignedClients : [];
    user.assignedProjects = user.assignedProjects ? user.assignedProjects : [];

    user
      .save()
      .then(function() {
        user
          .getRoles()
          .then(function(roles) {
            var roleArray = [];

            _.forEach(roles, function(role) {
              roleArray.push(role.dataValues.name);
            });

            user.dataValues.roles = roleArray;

            if(task === 'profile') {

              req.login(user, function (err) {

                if (err) {
                  res.status(400).send(err);
                } else {
                  res.json(user);
                }
              });

            }
            else{
              res.json(user);
            }

            return null;
          });

        return null;
      })
      .catch(function(err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });
  } else {
    res.status(400).send({
      message: 'User is not signed in'
    });
  }

}

/**
 * Update profile picture
 */
exports.changeProfilePicture = function(req, res) {
  var user = req.user;
  var upload = multer(config.uploads.profileUpload).single('newProfilePicture');
  var profileUploadFileFilter = require(path.resolve('./config/lib/multer')).profileUploadFileFilter;

  // Filtering to upload only images
  upload.fileFilter = profileUploadFileFilter;

  if (user) {
    upload(req, res, function(uploadError) {
      if (uploadError) {
        return res.status(400).send({
          message: 'Error occurred while uploading profile picture'
        });
      } else {
        user.profileImageURL = config.uploads.profileUpload.dest + req.file.filename;

        user
          .save()
          .then(function(user) {
            req.login(user, function(err) {
              if (err) {
                res.status(400).send(err);
              } else {
                res.json(user);
              }
            });

            return null;
          })
          .catch(function(err) {
            return res.status(400).send({
              message: errorHandler.getErrorMessage(err)
            });
          });
      }
    });
  } else {
    res.status(400).send({
      message: 'User is not signed in'
    });
  }
};

/**
 * Send User
 */
exports.me = function(req, res) {
  res.json(req.user || null);
};
