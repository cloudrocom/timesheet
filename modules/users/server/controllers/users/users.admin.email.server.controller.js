'use strict';

var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport('smtps://serban.nicusor0%40gmail.com:1z2x3casdqazwexc@smtp.gmail.com');

exports.sendMail = function(req, res) {

  var message = req.body.mail.message;
  var subject = req.body.mail.subject;
  var toEmail = req.body.mail.to;

  var mailOptions = {
    to: toEmail,
    from: req.user.email,
    subject: subject,
    html: message
  };

  transporter.sendMail(mailOptions, function(err) {
    if (!err) {
      res.send({
        message: 'sent to ' + toEmail
      });
    } else {
      return res.status(400).send({
        message: 'not sent to' + toEmail
      });
    }
  });

};