'use strict';

var path = require('path'),
  db = require(path.resolve('./config/lib/sequelize')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

exports.notificationCreate = function (req, res) {

  db.Notification.create({
    UserId: req.body.userId,
    title: req.body.title,
    message: req.body.message,
    type: req.body.type,
    seen: false
  })
    .then(function (newNotification) {
      return res.json(newNotification);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });
};

exports.notificationRead = function (req, res) {

  db.Notification.find({
    where: {
      id: req.params.id
    }
  })
    .then(function (notification) {
      return res.json(notification);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.notificationUpdate = function (req, res) {

  db.Notification
    .findOne({
      where: {
        id: req.body.id
      }
    })
    .then(function (notification) {
      notification.updateAttributes({

        title: req.body.title,
        message: req.body.message,
        type: req.body.type,
        seen: req.body.seen,
        updatedAt: 'NOW()'

      })
        .then(function () {
          return res.json(notification);
        })
        .catch(function (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });

      return null;
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.notificationDelete = function (req, res) {

  db.Notification
    .findOne({
      where: {
        id: req.params.id
      }
    })
    .then(function (notification) {
      notification.destroy()
        .then(function () {
          return res.json(notification);
        })
        .catch(function (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });

      return null;
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.notificationList = function (req, res) {

  var params = JSON.parse(req.query.params);

  db.Notification.findAll({
    where: {
      UserId: params.userId
    },
    include: [
      db.User
    ]
  })
    .then(function (notification) {
      return res.json(notification);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};


