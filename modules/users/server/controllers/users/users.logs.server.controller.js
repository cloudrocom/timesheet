'use strict';

var path = require('path'),
  db = require(path.resolve('./config/lib/sequelize')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

exports.logsCreate = function (req, res) {

  db.Logs.create({
    UserId: req.body.userId,
    action: req.body.action,
    parameters: req.body.parameters
  })
    .then(function (newLog) {
      return res.json(newLog);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });
};

exports.logsRead = function (req, res) {

  db.Logs.find({
    where: {
      id: req.params.id
    }
  })
    .then(function (log) {
      return res.json(log);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.logsUpdate = function (req, res) {

  db.Storage
    .findOne({
      where: {
        id: req.params.id
      }
    })
    .then(function (log) {
      log.updateAttributes({

        //A log should not be updated, never, ever lol

        //skillsMatrix: req.body.skillsMatrix,
        //cvPath: req.body.cvPath

      })
        .then(function () {
          return res.json(log);
        })
        .catch(function (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });

      return null;
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.logsDelete = function (req, res) {

  db.Logs
    .findOne({
      where: {
        id: req.params.id
      }
    })
    .then(function (log) {
      log.destroy()
        .then(function () {
          return res.json(log);
        })
        .catch(function (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });

      return null;
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.logsList = function (req, res) {

  var params = JSON.parse(req.query.params);

  db.Logs.findAll({
    where: {
      UserId: params.userId
    },
    include: [
      db.User
    ]
  })
    .then(function (logs) {
      return res.json(logs);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};


