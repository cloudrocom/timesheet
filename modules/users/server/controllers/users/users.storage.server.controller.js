'use strict';

var path = require('path'),
  db = require(path.resolve('./config/lib/sequelize')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

exports.storageCreate = function (req, res) {

  db.Storage.create({
    UserId: req.body.userId,
    skillsMatrix: req.body.skillsMatrix,
    cvPath: req.body.cvPath
  })
    .then(function (newStorage) {
      return res.json(newStorage);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });
};

exports.storageRead = function (req, res) {

  db.Storage.find({
    where: {
      id: req.params.id
    }
  })
    .then(function (storage) {
      return res.json(storage);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.storageUpdate = function (req, res) {

  db.Storage
    .findOne({
      where: {
        id: req.params.id
      }
    })
    .then(function (storage) {
      storage.updateAttributes({
        skillsMatrix: req.body.skillsMatrix,
        cvPath: req.body.cvPath
      })
        .then(function () {
          return res.json(storage);
        })
        .catch(function (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });

      return null;
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.storageDelete = function (req, res) {

  db.Storage
    .findOne({
      where: {
        id: req.params.id
      }
    })
    .then(function (storage) {
      storage.destroy()
        .then(function () {
          return res.json(storage);
        })
        .catch(function (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });

      return null;
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.storageList = function (req, res) {

  var params = JSON.parse(req.query.params);

  db.Storage.findAll({
    where: {
      UserId: params.userId
    },
    include: [
      db.User
    ]
  })
    .then(function (storage) {
      return res.json(storage);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};


