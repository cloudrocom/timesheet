'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');

/**
 * Extend user's controller
 */
module.exports = _.extend(

  require('./users/users.admin.server.controller'),
  require('./users/users.authentication.server.controller'),
  require('./users/users.authorization.server.controller'),
  require('./users/users.password.server.controller'),
  require('./users/users.profile.server.controller'),
  require('./users/users.storage.server.controller'),
  require('./users/users.notification.server.controller'),
  require('./users/users.logs.server.controller'),
  require('./users/users.admin.email.server.controller.js')

);
