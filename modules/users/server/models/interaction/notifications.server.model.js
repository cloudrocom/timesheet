'use strict';

module.exports = function(sequelize, DataTypes) {

  var Notification = sequelize.define('Notification', {

    title: {
      type: DataTypes.STRING
    },
    message: {
      type: DataTypes.STRING
    },
    type: {
      type: DataTypes.STRING
    },
    seen:{
      type: DataTypes.BOOLEAN
    }

  }, {

    classMethods: {
      associate: function(models) {
        Notification.belongsTo(models.User, {
          foreignKey: 'UserId',
          foreignKeyConstraint: true
        });
      }
    }

  });

  return Notification;
};
