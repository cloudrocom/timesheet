'use strict';

module.exports = function(sequelize, DataTypes) {

  var Logs = sequelize.define('Logs', {

    action: DataTypes.STRING,
    parameters: DataTypes.JSONB

  }, {

    classMethods: {

      associate: function(models) {
        Logs.belongsTo(models.User, {
          foreignKey: 'UserId',
          foreignKeyConstraint: true
        });
      }

    }

  });

  return Logs;
};
