'use strict';

module.exports = function(sequelize, DataTypes) {

  var Storage = sequelize.define('Storage', {

    skillsMatrix: {
      type: DataTypes.JSONB
    },
    cvPath: {
      type: DataTypes.STRING
    }

  }, {

    classMethods: {

      associate: function(models) {
        Storage.belongsTo(models.User, {
          foreignKey: 'UserId',
          foreignKeyConstraint: true
        });
      }

    }

  });

  return Storage;
};
