'use strict';

module.exports = function (app) {
  // User Routes
  var users = require('../controllers/users.server.controller');

  // Setting up the users profile api
  app.route('/api/users/me')
    .get(users.me);

  app.route('/api/users')
    .put(users.update);

  app.route('/api/users/accounts')
    .delete(users.removeOAuthProvider);

  app.route('/api/users/password')
    .post(users.changePassword);

  app.route('/api/users/picture')
    .post(users.changeProfilePicture);

  app.route('/api/users/storage/:id')
    .put(users.storageUpdate)
    .get(users.storageRead)
    .delete(users.storageDelete);

  app.route('/api/users/storage')
    .post(users.storageCreate)
    .get(users.storageList);

  app.route('/api/users/notifications/:id')
    .put(users.notificationUpdate)
    .get(users.notificationRead)
    .delete(users.notificationDelete);

  app.route('/api/users/notifications')
    .post(users.notificationCreate)
    .get(users.notificationList);

};
