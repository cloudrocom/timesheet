'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
  function ($stateProvider) {
    // Users state routing
    $stateProvider
      /* --- SETTINGS --- */
      .state('settings', {
        abstract: true,
        url: '/settings',
        templateUrl: 'modules/users/client/views/settings/settings.client.view.html',
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('settings.profile', {
        url: '/profile',
        templateUrl: 'modules/users/client/views/settings/edit-profile.client.view.html'
      })
      .state('settings.password', {
        url: '/password',
        templateUrl: 'modules/users/client/views/settings/change-password.client.view.html'
      })
      .state('settings.picture', {
        url: '/picture',
        templateUrl: 'modules/users/client/views/settings/change-profile-picture.client.view.html'
      })
      /* --- AUTHENTICATION --- */
      .state('authentication', {
        abstract: true,
        url: '/authentication',
        templateUrl: 'modules/users/client/views/authentication/authentication.client.view.html'
      })
      .state('authentication.signup', {
        url: '/signup',
        templateUrl: 'modules/users/client/views/authentication/signup.client.view.html'
      })
      .state('authentication.signin', {
        url: '/signin?err',
        templateUrl: 'modules/users/client/views/authentication/signin.client.view.html'
      })
      /* --- PASSWORD --- */
      .state('password', {
        abstract: true,
        url: '/password',
        template: '<ui-view/>'
      })
      .state('password.forgot', {
        url: '/forgot',
        templateUrl: 'modules/users/client/views/password/forgot-password.client.view.html'
      })
      .state('password.reset', {
        abstract: true,
        url: '/reset',
        template: '<ui-view/>'
      })
      .state('password.reset.invalid', {
        url: '/invalid',
        templateUrl: 'modules/users/client/views/password/reset-password-invalid.client.view.html'
      })
      .state('password.reset.success', {
        url: '/success',
        templateUrl: 'modules/users/client/views/password/reset-password-success.client.view.html'
      })
      .state('password.reset.form', {
        url: '/:token',
        templateUrl: 'modules/users/client/views/password/reset-password.client.view.html'
      })
      /* --- INFORMATION --- */
      .state('information', {
        abstract: true,
        url: '/information',
        template: '<ui-view/>'
      })
      .state('information.conduct', {
        url: '/conduct',
        controller: 'UserInformationConductController',
        controllerAs: 'vm',
        templateUrl: 'modules/users/client/views/information/information.conduct.client.view.html'
      })
      .state('information.procedures', {
        url: '/procedures',
        controller: 'UserInformationProceduresController',
        controllerAs: 'vm',
        templateUrl: 'modules/users/client/views/information/information.procedures.client.view.html'
      })
      .state('information.about', {
        url: '/about',
        controller: 'UserInformationAboutController',
        controllerAs: 'vm',
        templateUrl: 'modules/users/client/views/information/information.about.client.view.html'
      }).state('information.blog', {
        url: '/blog',
        controller: 'UserInformationBlogController',
        controllerAs: 'vm',
        templateUrl: 'modules/users/client/views/information/information.blog.client.view.html'
      }).state('information.events', {
        url: '/blog',
        controller: 'UserInformationEventsController',
        controllerAs: 'vm',
        templateUrl: 'modules/users/client/views/information/information.events.client.view.html'
      });
  }
]);
