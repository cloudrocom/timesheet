'use strict';

// Setting up route
angular.module('users.admin.routes').config(['$stateProvider', '$windowProvider',
  function ($stateProvider, $windowProvider) {

    $stateProvider
      .state('admin.users', {
        url: '/users',
        templateUrl: 'modules/users/client/views/admin/users/users.client.view.html',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.mailChimp', {
        url: '/mailChimp',
        templateUrl: 'modules/users/client/views/admin/users.mailchimp.client.view.html',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.edit', {
        url: '/edit',
        params: {
          user: null
        },
        controller: 'AdminEditUserController',
        controllerAs: 'vm',
        templateUrl: 'modules/users/client/views/admin/users/users.edit.client.view.html',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.email', {
        url: '/email',
        params: {
          user: null
        },
        controller: 'AdminEmailController',
        controllerAs: 'vm',
        templateUrl: 'modules/users/client/views/admin/users/users.email.client.view.html',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.notice', {
        url: '/notice',
        params: {
          user: null
        },
        controller: 'AdminNoticeController',
        controllerAs: 'vm',
        templateUrl: 'modules/users/client/views/admin/users/users.notice.client.view.html',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.statistics', {
        url: '/statistics',
        templateUrl: 'modules/users/client/views/admin/statistics.client.view.html',
        controller: 'AdminStatisticsController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.holidays', {
        url: '/holidays',
        templateUrl: 'modules/users/client/views/admin/holidays.client.view.html',
        controller: 'AdminHolidaysController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.notices', {
        url: '/notices',
        templateUrl: 'modules/users/client/views/admin/notices.client.view.html',
        controller: 'AdminNoticesController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.queue', {
        url: '/queue',
        templateUrl: 'modules/users/client/views/admin/queue.client.view.html',
        controller: 'AdminQueueController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.timesheets', {
        url: '/timesheets',
        templateUrl: 'modules/users/client/views/admin/timesheets.client.view.html',
        controller: 'AdminTimesheetsController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.clientProjects', {
        url: '/clientProjects',
        templateUrl: 'modules/users/client/views/admin/clientProjects.client.view.html',
        controller: 'AdminClientProjectsController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      });
  }
]);
