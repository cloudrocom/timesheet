'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', ['$resource',
  function ($resource) {
    return $resource('api/users', {}, {
      update: {
        method: 'PUT'
      },
      me: {
        url: 'api/users/me',
        method: 'GET',
        isArray: false
      }
    });
  }
]);

//TODO this should be Users service
angular.module('users.admin').factory('Admin', ['$resource',
  function ($resource) {
    return $resource('api/users/:userId', {
      userId: '@_id'
    }, {
      update: {
        method: 'PUT'
      },
      listUsers:{
        url:'/api/users/admin',
        method:'GET',
        isArray: false
      },
      getUser:{
        url:'/api/users/admin/:userId',
        method: 'GET',
        isArray: false
      }
    });
  }

]);
