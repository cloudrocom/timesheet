'use strict';

angular.module('users').factory('StorageService', ['$resource',
  function ($resource) {
    return $resource('api/users/storage', {}, {
      update: {
        method: 'PUT'
      },
      create: {
        method: 'POST'
      }/*,
      create: {
        url: 'api/storage/:id',
        method: 'GET',
        isArray: false
      }*/
    });
  }
]);