'use strict';

angular.module('users').factory('NotificationService', ['$resource',
  function ($resource) {

    var obj = this;

    obj.api = $resource('api/users/notifications', {

    }, {
      update: {
        url: '/api/users/notifications/:id',
        method: 'PUT'
      },
      create: {
        method: 'POST'
      }
    });

    obj.noticeTypes = ['Advertisement','Announce','Reminder','Urgent Reminder'];

    return obj;
  }
]);