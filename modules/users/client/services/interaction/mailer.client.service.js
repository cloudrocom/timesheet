'use strict';

angular.module('users.admin').factory('MailerService', ['$resource',
  function ($resource) {

    return $resource('/api/mail/:mail', {
      mail: '@mail'
    },
      {
        send:{
          method: 'POST'
        }
      });
  }
]);