'use strict';

angular.module('users').controller('ChangePasswordController', ['$scope', '$http', 'Authentication', 'PasswordValidator', 'ToastService', '$state',
  function ($scope, $http, Authentication, PasswordValidator, ToastService, $state) {

    $scope.user = Authentication.user;
    $scope.popoverMsg = PasswordValidator.getPopoverMsg();

    // Change user password
    $scope.changeUserPassword = function (isValid) {
      $scope.success = $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'passwordForm');

        return false;
      }

      $http.post('/api/users/password', $scope.passwordDetails).success(function (response) {
        // If successful show success message and clear form
        $scope.$broadcast('show-errors-reset', 'passwordForm');
        $scope.success = true;
        $scope.passwordDetails = null;

        ToastService.show('Password successfully changed!');
        $state.go($state.previous.state.name || 'home', $state.previous.params);

      }).error(function (response) {

        $scope.error = response.message;

        ToastService.show('Err: ' + response.message);

      });
    };
  }
]);
