'use strict';

angular.module('users').controller('ChangeProfilePictureController',
  ['$scope', '$timeout', '$window', 'Authentication', 'FileUploader', 'ToastService',
  function ($scope, $timeout, $window, Authentication, FileUploader, ToastService) {

    var vm = this;

    vm.authentication = Authentication;
    vm.imageURL = vm.authentication.user.profileImageURL;

    // Create file uploader instance
    vm.uploader = new FileUploader({
      url: 'api/users/picture',
      alias: 'newProfilePicture'
    });

    // Set file uploader image filter
    vm.uploader.filters.push({
      name: 'imageFilter',
      fn: function (item, options) {
        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
      }
    });

    // Called after the user selected a new picture file
    vm.uploader.onAfterAddingFile = function (fileItem) {

      if ($window.FileReader) {

        var fileReader = new FileReader();
        fileReader.readAsDataURL(fileItem._file);

        fileReader.onload = function (fileReaderEvent) {

          $timeout(function () {
            vm.imageURL = fileReaderEvent.target.result;
            vm.uploadProfilePicture();
          }, 0);

        };

      }

    };

    // Called after the user has successfully uploaded a new picture
    vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
      // Show success message
      vm.success = true;

      // Populate user object
      Authentication.user = response;
      vm.authentication = Authentication;

      // Clear upload buttons
      vm.cancelUpload();

      ToastService.show('Image uploaded successfully!');
    };

    // Called after the user has failed to uploaded a new picture
    vm.uploader.onErrorItem = function (fileItem, response, status, headers) {
      // Clear upload buttons
      vm.cancelUpload();

      // Show error message
      vm.error = response.message;

      ToastService.show('Something bad happened on the way ...');
    };

    // Change user profile picture
    vm.uploadProfilePicture = function () {
      // Clear messages
      vm.success = $scope.error = null;

      // Start upload
      vm.uploader.uploadAll();
    };

    // Cancel the upload process
    vm.cancelUpload = function () {
      vm.uploader.clearQueue();
      vm.imageURL = vm.authentication.user.profileImageURL;
    };
  }
]);
