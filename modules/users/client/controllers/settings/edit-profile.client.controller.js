'use strict';

angular.module('users').controller('EditProfileController',
  ['$scope', '$http', '$location', 'Users', 'Authentication', 'StorageService', '$mdToast', 'ToastService', '$state',

    function ($scope, $http, $location, Users, Authentication, StorageService, $mdToast, ToastService, $state) {

      var vm = this;
      $scope.user = Authentication.user;
      vm.userStorage = null;

      //TODO
      //notifications model +
      //upload CV here

      vm.getAllFromStorage = function(){

        StorageService.query({
          params:{
            userId: vm.authentication.user.id
          }
        }).$promise.then(function (storageObjects) {

          vm.userStorage = JSON.parse(JSON.stringify(storageObjects));
            //do stuff here after it's all done
        });

      };

      vm.insertIntoStorage = function(){

        StorageService.create({

          userId:vm.authentication.user.id,
          skillsMatrix: vm.userStorage.skillsMatrix,
          cvPath: vm.userStorage.cvPath

        }).$promise.then(function (storageObject) {

          vm.userStorage = JSON.parse(JSON.stringify(storageObject));
            //do stuff here after it's all done
        });

      };

      // Update a user profile
      $scope.updateUserProfile = function (isValid) {

        $scope.success = $scope.error = null;

        if (!isValid) {
          $scope.$broadcast('show-errors-check-validity', 'userForm');

          return false;
        }

        var user = new Users($scope.user);

        user.$update(function (response) {

          Authentication.user = response;
          ToastService.show('Profile Saved Successfully');
          $state.go($state.previous.state.name || 'home', $state.previous.params);

        }, function (response) {

          console.log(response);
          ToastService.show('Err: ' + response.data.message);

        });

      };

    }
  ]);

/*

 var skillsMatrix = {
 systemOs:{
 osAdministration: {},
 virtualizationTechnology: {},
 hardwarePlatform: {}
 },
 programmingLanguages:{
 scripting: {},
 microsoftTehnologies: {},
 javaJ2ee: {},
 developmentOnSpecificOs: {},
 developmentOnSpecificServers: {},
 database: {},
 sap: {},
 cobol: {},
 hra: {},
 webDevelopment: {},
 otherProgrammingLanguages: {}
 },
 database:{
 rdbmsDevelopment: {},
 rdbmsAdministration: {},
 oracleDeveloperSuite: {},
 dataWarehousing: {},
 ibmInfoSphereDatastage: {}
 },
 toolsMethodsOther:{
 networking:{},
 testing:{
 testingGeneric: {},
 testingTools: {}
 },
 developmentTools: {},
 developmentDesignTehniques: {},
 developmentOnSpecificAreas: {},
 serverAdministration: {
 administration:{},
 microsoftProject: {}
 },
 managementLeadership: {},
 developmentMethodologies: {},
 industryExperience: {}
 },
 languages: {}
 };


 * */