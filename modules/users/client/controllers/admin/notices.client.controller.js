'use strict';

angular.module('users.admin').controller('AdminNoticesController',
  ['Authentication', 'Users', 'moment', '_', '$http', '$rootScope', '$scope', '$stateParams', '$location', 'NotificationService',

  function(Authentication, Users, moment, _, $http, $rootScope, $scope, $stateParams, $location, NotificationService) {

    var vm = this;
    vm.user = Authentication.user;

    $scope.authentication = Authentication;

    // Authentication check
    if (!$scope.authentication.user) {
      $location.path('/authentication/signin');
    } else {
      var roles = $scope.authentication.user.roles;

      if (_.includes(roles, 'admin')) {
        $scope.authenticated = true;
      } else {
        $location.path('/');
      }
    }

    $scope.test = 'asdqaz';
    console.log('mdfkr');

    //this will be used to send notices (and emails?)

    vm.createNotice = function() {

      NotificationService.create({

      },{

        userId: vm.user.id, // The userid of the one who shall receive the notice !!!
        title: 'test', // notice title
        message: 'message', // notice message
        type: 'success' // notice type check [ http://janstevens.github.io/angular-growl-2/ ]


      }).$promise.then(function (notifications) {

          //HEre you shall receive the new notice object ( row ) in the db

      });

    };

  }
]);
