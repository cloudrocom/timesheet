'use strict';

angular.module('users.admin').controller('AdminClientProjectsController', ['Authentication', 'Users', 'moment', '_', '$http', '$rootScope', '$scope', '$stateParams', '$location',

  function(Authentication, Users, moment, _, $http, $rootScope, $scope, $stateParams, $location) {

    $scope.authentication = Authentication;

    // Authentication check
    if (!$scope.authentication.user) {
      $location.path('/authentication/signin');
    } else {
      var roles = $scope.authentication.user.roles;

      if (_.includes(roles, 'admin')) {
        $scope.authenticated = true;
      } else {
        $location.path('/');
      }
    }

    $scope.test = 'asdqaz';
    console.log('mdfkr');

    //TODO
    // fetch all clients
    // for each client fetch all projects
    // display minimal information

    //when a client is clicked it will go in a new view or modal or whatever
    //then, fetch all the timesheet data for that client for all the projects and display them there

    //i'd suggest using an filled array object with all the clients
    // on client click populate that exact object from array with aditional properties

  }
]);
