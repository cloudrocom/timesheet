'use strict';

angular
  .module('users.admin', [])
  .controller('AdminQueueController',
  ['Authentication', 'Users', 'GlobalService', 'HolidaysService', 'TimesheetsService', 'NotificationService',
    'moment', '_', '$http', '$rootScope', '$scope', '$stateParams', '$location', '$log', '$timeout', '$mdpTimePicker',
    '$mdDialog', 'ToastService', 'ProjectsService', 'screenSize', '$window', '$q', '$mdEditDialog',

    function(Authentication, Users, GlobalService, HolidaysService, TimesheetsService, NotificationService,
             moment, _, $http, $rootScope, $scope, $stateParams, $location, $log, $timeout, $mdpTimePicker,
             $mdDialog, ToastService, ProjectsService, screenSize, $window, $q, $mdEditDialog) {

      $scope.authentication = Authentication;
      $scope.objects = [];
      $scope.log = $log;

      fetchData();

      var weekday = GlobalService.weekDays;
      $scope.displayNameWidth = isMobile() ? '80px' : '120px';
      $scope.dayEntryAlign = isMobile() ? 'column' : 'row';

      /* --- Responsive Related --- */

      function isMobile(){
        return screenSize.is('xs, sm');
      }

      //$scope.weekDaysLayout = isMobile() ? 'column' : 'row';

      var w = angular.element($window);
      $scope.$watch(
        function () {
          return $window.innerWidth;
        },
        function (value) {

          $scope.displayNameWidth = isMobile() ? '80px' : '120px';
          $scope.dayEntryAlign = isMobile() ? 'column' : 'row';

          $scope.windowWidth = value;
        },
        true
      );

      w.bind('resize', function(){
        $scope.$apply();
      });

      /* --- Responsive Related --- */

      $scope.timesheets = true;
      $scope.workDayTypes = TimesheetsService.workDayTypes;

      $scope.holidays = false;

      //init();

      $scope.accept = function (object) {

        object.showAction = false;

        if (object.type === 'holiday') {

/*
          $scope.holidaysObjects[index].accepted = true; // Sets it to accepted

          HolidaysService.update({
              id: $scope.holidaysObjects[index].id
            },
            $scope.holidaysObjects[index] // sends the updated object for update
          )
            .$promise.then(function (data) {
              // updates middleware object
              $scope.holidaysObjects[index] = JSON.parse(JSON.stringify(data));
            });
*/

        }
        else if (object.type === 'timesheet') {

          object.data.accepted = true;
          object.data.ProjectId = object.data.Project.id;
          object.data.date = GlobalService.addDays(object.data.date,1);

          delete object.data.User;
          delete object.data.Project;

          TimesheetsService.api.update({
            id: object.data.id
          },object.data)
            .$promise.then(function (data) {
              ToastService.show('Entry accepted!', 1000);
              //getTimesheets();
            });

        }

      };

      $scope.deny = function (object) {
        //TODO Send a notice to the user so he can edit the current entry because it is invalid

        object.data.accepted = true;

        if (object.type === 'holiday') {

/*          $scope.holidaysObjects[index].accepted = true; // Sets it to accepted

          HolidaysService.update({
              id: $scope.holidaysObjects[index].id
            },
            $scope.holidaysObjects[index] // sends the updated object for update
          )
            .$promise.then(function (data) {
              // updates middleware object
              $scope.holidaysObjects[index] = JSON.parse(JSON.stringify(data));
            });*/

        }
        else if (object.type === 'timesheet') {


          object.data.accepted = false;
          object.data.requiresEdit = true;

          delete object.data.User;
          delete object.data.Project;

          TimesheetsService.api.update({
            id: object.data.id
          },object.data)
            .$promise.then(function (data) {
              ToastService.show('Entry denied!', 1000);
              //getTimesheets();
            });

        }

      };

      $scope.edit = function (type, index) {
        // type = holiday or timesheet, i'd go for two separate data container fields
        // index = index of the updated element in the data array

        if (type === 'holiday') {

          $scope.holidaysObjects[index].accepted = true; // Sets it to accepted

          HolidaysService.update({
            id: $scope.holidaysObjects[index].id
          },
            $scope.holidaysObjects[index] // sends the updated object for update
          )
            .$promise.then(function (data) {
              // updates middleware object
              $scope.holidaysObjects[index] = JSON.parse(JSON.stringify(data));
            });

        }
        else if (type === 'timesheet') {

          $scope.timesheetsObjects[index].accepted = true; // Sets it to accepted

          TimesheetsService.update({
            id: $scope.timesheetsObjects[index].id
          },
            $scope.timesheetsObjects[index]// sends the updated object for update
          )
            .$promise.then(function (data) {
              // updates middleware object
              $scope.timesheetsObjects[index] = JSON.parse(JSON.stringify(data));
            });

        }

      };

      $scope.searchForTimesheets = function () {
        $scope.timesheets = true;
        $scope.holidays = false;
        init();
      };

      $scope.searchForHolidays = function () {
        $scope.timesheets = false;
        $scope.holidays = true;

        init();
      };

      $scope.getDateFromString = function (dateAsString) {
        var date = new Date(dateAsString);
        return date;
      };

      function init() {

        $scope.loaded = false;

        if ($scope.timesheets) {
          //getTimesheets();
        }
        else if ($scope.holidays) {
          getHolidays();
        }

      }

      function getHolidays() {

        HolidaysService.queue({
          userId: 'all',
          limit: $scope.query.limit,
          offset: ($scope.query.page - 1) * $scope.query.limit
        }) // Used to populate entire calendar with all the holidays data at first
          .$promise.then(function (data) {
            // gets the data, takes it out into stringify, transforms it onto an javascript object.

            $scope.objects = [];
            for (var index in data.rows) {

              $scope.objects.push({
                type: 'holiday',
                data: data.rows[index]
              });

            }

            $scope.totalItems = data.count;
            $scope.loaded = true;

          });

      }

      /* --- TIME PICKERS --- */

      $scope.filterDate = function (date) {
        return moment(date).date() % 2 === 0;
      };

      $scope.showTimePicker = function (ev, type, entry) {

        console.log(entry.fromHours);
        console.log(entry.toHours);

        if (type === 'fromHours') {
          $mdpTimePicker(new Date('11-11-2011 ' + entry.fromHours), {
            targetEvent: ev
          }).then(function (selectedDate) {
            entry.fromHours = selectedDate;
          });
        }
        else if (type === 'toHours') {
          $mdpTimePicker(new Date('11-11-2011 ' + entry.toHours), {
            targetEvent: ev
          }).then(function (selectedDate) {
            entry.toHours = selectedDate;
          });
        }

      };

      /* --- HELPER FUNCTIONS --- */

      $scope.getFirstPart = function(textString, charactersNumber){

        var returnString = '';

        var indexer = 0;
        for(var stringIndex in textString){

          if(indexer === charactersNumber) break;

          returnString += textString[stringIndex];
          indexer++;
        }

        for(var i = 0; i < 3; i++){
          returnString += '.';
        }

        return returnString;
      };

      /* --- Task Dialogue --- */

      $scope.showPrompt = function (ev, object) {

        var confirm = $mdDialog.prompt()
          .title('You\'re now editing the current entry task, please make sure everything is correct before saving!')
          .clickOutsideToClose(true)
          .placeholder('Edit current task ...')
          .ariaLabel('Edit current task ...')
          .initialValue(object.data.task)
          .targetEvent(ev)
          .ok('Save')
          .cancel('Cancel');

        $mdDialog.show(confirm).then(function(result) {
          //Save
          object.data.task = result;
        }, function() {
          //Cancel
        });

      };

      /* --- INFINITE SCROLL --- */

      $scope.selectedWeekObjectKey = null;
      $scope.collapseWeekAction = function(item, key, val){

        if($scope.selectedWeekObjectKey !== key){
          $scope.selectedWeekObjectKey = key;
          item.collapse = true;
        }
        else{
          item.collapse = item.collapse ? false : true;
        }

      };

      $scope.collapseDay = function(dayObject){

        dayObject.collapse = dayObject.collapse ? false : true;

      };

      $scope.acceptDay = function (obj){
        console.log(obj);
      };

      var currentUsers = [];

      $scope.objects.get = function (descriptor, success) {

        return $timeout(function () {

          if(currentUsers.length !== 0)
            return success([]);

          var count, index, response;
          index = descriptor.index; // not used
          count = descriptor.count; // not used
          response = [];

          TimesheetsService.api.queue({
            userId: 'all',
            limit: count,
            offset: index <= -1 ? 0 : index

          }) // Used to populate entire calendar with all the holidays data at first
            .$promise.then(function (data) {
              // gets the data, takes it out into stringify, transforms it onto an javascript object.

              var memoryObj = [];

              for (var index in data.rows) {

                if(GlobalService.getIndex(data.rows[index].User.id,currentUsers) !== -1)
                  continue;

                var user = data.rows[index].User;
                var project = data.rows[index].Project;
                var timesheetEntry = data.rows[index];
                var timesheetEntryDate = timesheetEntry.date.toString();
                var weekNumber = new Date(timesheetEntry.date).getWeekNumber();

                var uuid = user.id + '-' + user.email;

                delete timesheetEntry.User;
                delete timesheetEntry.Project;

                if(memoryObj[uuid] === undefined)
                  memoryObj[uuid] = {};

                memoryObj[uuid].user = user;
                memoryObj[uuid].collapse = false;

                if (memoryObj[uuid].entries === undefined)
                  memoryObj[uuid].entries = {};

                if (memoryObj[uuid].entries[weekNumber.toString()] === undefined)
                  memoryObj[uuid].entries[weekNumber.toString()] = {};

                if (memoryObj[uuid].entries[weekNumber.toString()][timesheetEntryDate] === undefined)
                  memoryObj[uuid].entries[weekNumber.toString()][timesheetEntryDate] = {};

                memoryObj[uuid].entries[weekNumber.toString()][timesheetEntryDate].collapse = false;

                if (memoryObj[uuid].entries[weekNumber.toString()][timesheetEntryDate].data === undefined)
                  memoryObj[uuid].entries[weekNumber.toString()][timesheetEntryDate].data = [];

                memoryObj[uuid].entries[weekNumber.toString()][timesheetEntryDate].data.push({
                  entry: timesheetEntry,
                  project: project
                });

              }

              for(var xa in memoryObj){
                currentUsers.push(memoryObj[xa].user.id);
                response.push(memoryObj[xa]);
                //console.log(memoryObj[xa]);
              }

              return success(index <= -1 ? [] : response);

            });

        }, 250);

      };


      /* --- --- --- --- --- */

      function fetchData (){

        var limit = 10;
        var offset =0;

        TimesheetsService.api.queue({

          userId: 'all',
          limit: limit,
          offset: offset

        }) // Used to populate entire calendar with all the holidays data at first
          .$promise.then(function (data) {
          // gets the data, takes it out into stringify, transforms it onto an javascript object.

            $scope.objects = data;

            console.log(data);

          });

      }

      /* --- MD DATA TABLE --- */


      $scope.selected = [];
      $scope.limitOptions = [5, 10, 15];

      $scope.options = {

        rowSelection: false,
        multiSelect: true,
        autoSelect: true,
        decapitate: false,
        largeEditDialog: false,
        boundaryLinks: false,
        limitSelect: true,
        pageSelect: true

      };

      $scope.query = {
        order: 'name',
        limit: 5,
        page: 1,
        search: ''
      };

      $scope.editComment = function (event, dessert) {
        event.stopPropagation(); // in case autoselect is enabled

        var editDialog = {
          modelValue: dessert.comment,
          placeholder: 'Add a comment',
          save: function (input) {

            if(input.$modelValue === 'Donald Trump') {
              input.$invalid = true;
              return $q.reject();
            }

            if(input.$modelValue === 'Bernie Sanders') {
              dessert.comment = 'FEEL THE BERN!';
              return true;
            }

            dessert.comment = input.$modelValue;
          },
          targetEvent: event,
          title: 'Add a comment',
          validators: {
            'md-maxlength': 30
          }
        };

        var promise;

        if($scope.options.largeEditDialog) {
          promise = $mdEditDialog.large(editDialog);
        } else {
          promise = $mdEditDialog.small(editDialog);
        }

        promise.then(function (ctrl) {
          var input = ctrl.getInput();

          input.$viewChangeListeners.push(function () {
            input.$setValidity('test', input.$modelValue !== 'test');
          });
        });
      };

      $scope.toggleLimitOptions = function () {
        $scope.limitOptions = $scope.limitOptions ? undefined : [5, 10, 15];
      };

      $scope.getTypes = function () {
        return ['Candy', 'Ice cream', 'Other', 'Pastry'];
      };

      $scope.loadStuff = function () {
        $scope.promise = $timeout(function () {
          // loading
        }, 2000);
      };

      $scope.logItem = function (item) {
        console.log(item, 'was selected');
      };

      $scope.logOrder = function (order) {
        console.log('order: ', order);
      };

      $scope.logPagination = function (page, limit) {
        console.log('page: ', page);
        console.log('limit: ', limit);
      };


      /* --- TESTS --- */

      $scope.getDateFromString = function(dateAsString){
        return new Date(dateAsString);
      };

      $scope.testxzxz = true;
      $scope.testxaxa = function(){
        $scope.testxzxz = $scope.testxzxz ? false : true;
      };

    }
  ]);
