'use strict';

angular.module('users.admin').controller('AdminHolidaysController', ['Authentication', 'Users', 'moment', '_', '$http', '$rootScope', '$scope', '$stateParams', '$location',
  'HolidaysService', 'NotificationService', 'GlobalService',

  function(Authentication, Users, moment, _, $http, $rootScope, $scope, $stateParams, $location,
           HolidaysService, NotificationService, GlobalService) {

    var vm = this;
    vm.user = Authentication.user;

    vm.holidaysObjects = null;

    vm.accept = function(selectedObject){

      selectedObject.accepted = true;

      HolidaysService.update({
        id: selectedObject.id
      },selectedObject) // Sends the entire object as a parameter for update
        .$promise.then(function (data) {
          vm.holidaysObjects[GlobalService.getIndex(selectedObject,vm.holidaysObjects)] = JSON.parse(JSON.stringify(data));
        });

    };

    vm.deny = function(userObject){

      //TODO ask Front-End
      // Shall we do a 'denied' check on it ?

      NotificationService.create({

        title: 'Holidays Sheet entry error!',
        message: 'Your Holiday Sheet entry is invalid, please make sure it\'s correct!',
        type: 'warning',
        seen: false,
        userId: userObject.id

      }).$promise.then(function (data) {
        var responseObject = JSON.parse(JSON.stringify(data));
      });

    };

    vm.edit = function(selectedObject){

      //handle object editing in a modal or something

      HolidaysService.update({
        id: selectedObject.id
      },selectedObject) // Sends the entire object as a parameter for update
        .$promise.then(function (data) {
          vm.holidaysObjects[GlobalService.getIndex(selectedObject,vm.holidaysObjects)] = JSON.parse(JSON.stringify(data));
        });

    };

    HolidaysService.queue({
      userId: 'all'
    })
      .$promise.then(function (data) {
        vm.holidaysObjects = JSON.parse(JSON.stringify(data)); // fills the middleware object with all the holidays in queue
      });

  }
]);
