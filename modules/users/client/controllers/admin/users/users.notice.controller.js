/*jshint loopfunc: true */
'use strict';

angular.module('users.admin').controller('AdminNoticeController', [
  'Authentication','Users','moment','_','$http','$scope','$stateParams', '$q', '$timeout', 'ToastService', '$state', 'Admin', 'NotificationService',

  function(Authentication,Users,moment,_,$http,$scope,$stateParams, $q, $timeout, ToastService, $state, Admin, NotificationService) {

    if(!$stateParams.user)
      $state.go('admin.users');

    $scope.title = null;
    $scope.message = null;
    $scope.noticeType = null;
    $scope.user = $stateParams.user;
    $scope.noticeTypes = NotificationService.noticeTypes;

    $scope.sendNotice = function(){

      for (var i = 0; i < $scope.contacts.length; i++) {

        NotificationService.api.create({

          userId: $scope.contacts[i].id,
          title: $scope.title,
          message: $scope.message,
          type: $scope.noticeType

        }).$promise.then(function (response) {

          var result = JSON.parse(JSON.stringify(response));

          if(result){
            ToastService.show('A notice was created.');
          }
          else{
            ToastService.show('A notice was not created.');
          }

        });
      }

      $state.go('admin.users');

    };

    /* --- CARDS LOGIC (SELECT RECIPIENTS) --- */

    $scope.allContacts = loadContacts();

    $scope.filterSelected = true;
    $scope.querySearch = querySearch;

    $scope.contacts = [{

      id: $scope.user.id,
      name: $scope.user.displayName,
      image: $scope.user.profileImageURL

    }];


    function querySearch (criteria) {
      return $scope.allContacts.filter(createFilterFor(criteria));
    }

    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(contact) {
        return (contact._lowername.indexOf(lowercaseQuery) !== -1);
      };

    }

    function loadContacts() {

      Admin.listUsers({})
        .$promise.then(function (data) {

          var contacts = JSON.parse(JSON.stringify(data));

          var response = contacts.rows.map(function (user, index) {

            var contact = {
              id: user.id,
              name: user.displayName,
              image: user.profileImageURL
            };

            contact._lowername = contact.name.toLowerCase();
            return contact;

          });

          $scope.allContacts = response;
        });
    }

  }
]);
