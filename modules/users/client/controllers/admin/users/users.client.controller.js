'use strict';

angular.module('users').controller('UsersController', [
  'Authentication','Users','moment','_','$http','$rootScope','$scope','$stateParams','$location', '$q', '$timeout', 'ToastService', '$state', 'DialogueService',

  function(Authentication,Users,moment,_,$http,$rootScope,$scope,$stateParams,$location, $q, $timeout, ToastService, $state, DialogueService) {

    var searchTimer;

    $scope.authentication = Authentication;
    $scope.rolesList = undefined;

    $scope.query = {
      limit: 10,
      page: 1,
      search: ''
    };

    $scope.options = {
      rowSelection: false,
      multiSelect: false,
      autoSelect: false,
      decapitate: false,
      largeEditDialog: false,
      boundaryLinks: false,
      limitSelect: false,
      pageSelect: false
    };

    if (_.includes($scope.authentication.user.roles, 'admin')) {
      $scope.authenticated = true;
    } else {
      $location.path('/');
    }

    /* --- SEARCH AND QUERY --- */

    $scope.action = function(action, user, event){

      switch(action) {

        case 'edit':
          $scope.edit(user);
          break;
        case 'remove':
          $scope.remove(user, event);
          break;
        case 'sendEmail':
          $scope.sendEmail(user);
          break;
        case 'sendNotice':
          $scope.sendNotice(user);
          break;

      }

    };

    $scope.sendNotice = function (user){

      $state.go('admin.notice', {
        user: user
      });

    };

    $scope.sendEmail = function (user){

      $state.go('admin.email', {
        user: user
      });

    };

    $scope.remove = function(user, event) {

      DialogueService.confirmDialogue('Delete ' + user.displayName + ' ?',
        'This will erase the user entirely, no recover possible!',
        'Delete User Dialogue',
        event,
        'Okay',
        'Cancel',
        function(){
          /* --- Confirm function --- */
          if (user) {

            $http({
              url: 'api/users/admin/' + user.id,
              method: 'DELETE'
            }).success(function(data) {
              ToastService.show(user.displayName + ' was deleted!');
              $scope.find();
            });

          } else {
            $scope.user.$remove(function() {
              ToastService.show(user.displayName + ' was deleted!');
              $scope.find();
            });
          }

        },
        function(){
          /* --- Cancel function --- */
        });

    };

    $scope.edit = function(user){

      $state.go('admin.edit', {
        user: user
      });

    };



    $scope.setRole = function(user, role, event){

      DialogueService.confirmDialogue('Make ' + user.displayName + ' an ' + role.name + ' ?',
        'This will set ' + user.displayName + ' as an ' + role.name + ', and give him/her the rights to do more or less around.',
        'Change Role Dialogue',
        event,
        'Okay',
        'Cancel',
        function(){
          /* --- Confirm function --- */
          $http({
            url: 'api/users/admin/' + user.id,
            method: 'PUT',
            params: {
              roles: [role.id]
            }
          })
            .success(function(data) {
              ToastService.show(user.displayName + ' is now an ' + role.name + ' !');
            });

        },
        function(){
          /* --- Cancel function --- */
        });

    };

    $scope.find = function() {

      if($scope.rolesList === undefined){

        $http({
          url: 'api/users/roles',
          method: 'GET'
        })
          .success(function(data) {
            $scope.rolesList = data;
          });
      }

      var limit = $scope.query.limit;
      var offset = ($scope.query.page - 1) * $scope.query.limit;
      var search = $scope.query.search;

      var params = {
        'limit': limit,
        'offset': offset,
        'search': search
      };

      $scope.loaded = false;

      $http({
        url: 'api/users/admin',
        method: 'GET',
        params: params
      }).success(function(data) {

        $scope.totalItems = data.count;
        $scope.users = data.rows;

        $scope.loaded = true;

      });

    };

    $scope.search = function(){

      if(searchTimer !== undefined)
        $timeout.cancel(searchTimer);

      searchTimer = $timeout(function () {
        $scope.find();
      }, 500);

    };

    $scope.logItem = function (item) {

    };

    $scope.logOrder = function (order) {

      /* Not used, no idea why this is still here tho.*/
      $scope.promise = $timeout(function () {
        $scope.find();
      }, 250);

    };

    $scope.logPagination = function (page, limit) {

      $scope.promise = $timeout(function () {
        $scope.find();
      }, 250);

    };

    /* --- FAB --- */

    $scope.hidden = false;
    $scope.isOpen = false;
    $scope.hover = false;

    $scope.$watch('isOpen', function(isOpen) {

      if (isOpen) {

        $timeout(function() {
          $scope.tooltipVisible = $scope.isOpen;
        }, 600);

      } else {
        $scope.tooltipVisible = $scope.isOpen;
      }

    });

    $scope.items = [
      { name: 'Send Email', icon: 'icons/communication/ic_email_48px.svg', direction: 'bottom', action:'sendEmail' },
      { name: 'Send Notice', icon: 'icons/communication/ic_chat_bubble_outline_48px.svg', direction: 'bottom', action:'sendNotice' },
      { name: 'Edit User', icon: 'icons/editor/ic_mode_edit_48px.svg', direction: 'bottom', action:'edit' },
      { name: 'Remove User', icon: 'icons/content/ic_delete_sweep_48px.svg', direction: 'bottom', action:'remove' }
    ];

  }
]);
