/*jshint loopfunc: true */
'use strict';

angular.module('users.admin').controller('AdminEmailController', [
  '$scope','$stateParams','$location', '$q', '$timeout', 'ToastService', '$state', 'MailerService', 'Admin',

  function($scope,$stateParams,$location, $q, $timeout, ToastService, $state, MailerService, Admin) {

    if(!$stateParams.user)
      $state.go('admin.users');

    $scope.user = $stateParams.user;
    $scope.mail = {
      subject: null,
      message: null
    };

    $scope.sendEmail = function(){

      for (var i = 0; i < $scope.contacts.length; i++) {

        MailerService.send({
          mail: {
            subject: $scope.mail.subject,
            message: $scope.mail.message,
            to: $scope.contacts[i].email
          }
        }).$promise.then(function (response) {
          ToastService.show('An email was ' + JSON.parse(JSON.stringify(response)).message + ' .');
        });

      }

      $state.go('admin.users');
    };

    /* --- CARDS LOGIC (SELECT RECIPIENTS) --- */

    $scope.allContacts = loadContacts();
    $scope.contacts = [{
      name: $scope.user.displayName,
      email: $scope.user.email,
      image: $scope.user.profileImageURL
    }];
    $scope.filterSelected = true;
    $scope.querySearch = querySearch;

    function querySearch (criteria) {
      return $scope.allContacts.filter(createFilterFor(criteria));
    }

    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(contact) {
        return (contact._lowername.indexOf(lowercaseQuery) !== -1);
      };

    }

    function loadContacts() {

      Admin.listUsers({})
        .$promise.then(function (data) {

          var contacts = JSON.parse(JSON.stringify(data));

          var response = contacts.rows.map(function (user, index) {

            var contact = {
              name: user.displayName,
              email: user.email,
              image: user.profileImageURL
            };

            contact._lowername = contact.name.toLowerCase();
            return contact;

          });

          $scope.allContacts = response;
        });
    }

  }
]);
