'use strict';

angular.module('users.admin').controller('AdminEditUserController',
  ['$scope', '$http', '$location', 'Users', 'Authentication', 'StorageService', 'ToastService', '$state', '$stateParams',

    function ($scope, $http, $location, Users, Authentication, StorageService, ToastService, $state, $stateParams) {

      if(!$stateParams.user)
        $state.go('admin.users');

      $scope.user = $stateParams.user;

      $scope.updateUserProfile = function (isValid) {

        $scope.success = $scope.error = null;

        if (!isValid) {
          $scope.$broadcast('show-errors-check-validity', 'userForm');

          return false;
        }

        var user = new Users($scope.user);

        user.$update(function (response) {

          ToastService.show('User was edited successfully');

          $state.go('admin.users');

        }, function (response) {

          ToastService.show('Err: ' + response.data.message);

        });

      };

    }
  ]);