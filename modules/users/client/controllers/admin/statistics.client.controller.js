'use strict';

angular.module('users.admin').controller('AdminStatisticsController', ['Authentication', 'Users', 'moment', '_', '$http', '$rootScope', '$scope', '$stateParams', '$location',

  function(Authentication, Users, moment, _, $http, $rootScope, $scope, $stateParams, $location) {

    $scope.authentication = Authentication;

    // Authentication check
    if (!$scope.authentication.user) {
      $location.path('/authentication/signin');
    } else {
      var roles = $scope.authentication.user.roles;

      if (_.includes(roles, 'admin')) {
        $scope.authenticated = true;
      } else {
        $location.path('/');
      }
    }

    $scope.test = 'asdqaz';
    console.log('mdfkr');

    //TODO ask Marina about these ones
  }
]);
