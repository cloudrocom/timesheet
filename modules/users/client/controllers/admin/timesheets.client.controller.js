'use strict';

angular.module('users.admin').controller('AdminTimesheetsController',
  ['Authentication', 'Users', 'moment', '_', '$http', '$rootScope', '$scope', '$stateParams', '$location',
    'TimesheetsService', 'NotificationService', 'GlobalService',

  function(Authentication, Users, moment, _, $http, $rootScope, $scope, $stateParams, $location,
           TimesheetsService, NotificationService, GlobalService) {

    var vm = this;
    vm.user = Authentication.user;

    vm.timesheetsObjects = null;

    vm.accept = function(selectedObject){

      selectedObject.accepted = true;

      TimesheetsService.update({
        id: selectedObject.id
      },selectedObject) // Sends the entire object as a parameter for update
      .$promise.then(function (data) {
        vm.timesheetsObjects[GlobalService.getIndex(selectedObject,vm.timesheetsObjects)] = JSON.parse(JSON.stringify(data));
      });

    };

    vm.deny = function(userObject){

      //TODO ask Front-End
      // Shall we do a 'denied' check on it ?

      NotificationService.create({

        title: 'Time Sheet entry error!',
        message: 'Your Time Sheet entry is invalid, please make sure it\'s correct!',
        type: 'warning',
        seen: false,
        userId: userObject.id

      }).$promise.then(function (data) {
        var responseObject = JSON.parse(JSON.stringify(data));
      });

    };

    vm.edit = function(selectedObject){

      //handle object editing in a modal or something

      TimesheetsService.update({
        id: selectedObject.id
      },selectedObject) // Sends the entire object as a parameter for update
        .$promise.then(function (data) {
          vm.timesheetsObjects[GlobalService.getIndex(selectedObject, vm.timesheetsObjects)] = JSON.parse(JSON.stringify(data));
        });

    };

    TimesheetsService.queue({
      userId: 'all'
    })
      .$promise.then(function (data) {
        vm.timesheetsObjects = JSON.parse(JSON.stringify(data)); // fills the middleware object with all the timesheets in queue
      });

  }
]);
