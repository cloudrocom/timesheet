'use strict';

angular.module('users').controller('AuthenticationController',
  ['$scope', '$state', '$http', '$location', '$window', 'Authentication', 'PasswordValidator', 'menu', 'ToastService',
  function (
    $scope, $state, $http, $location, $window, Authentication, PasswordValidator, menu, ToastService) {

    $scope.authentication = Authentication;
    $scope.popoverMsg = PasswordValidator.getPopoverMsg();

    var quotes = [{
      author: 'Oliver Wendell Holmes',
      message: 'All limitations are self imposed.'
    },{
      author: 'Kenny Rogers',
      message: 'Do not be afraid to give up the good for the great.'
    },{
      author: 'Henri Bergson',
      message: 'To exist is to change. To change is to mature. To mature is to go on creating oneself endlessly.'
    },{
      author: 'Lao Tzu',
      message: 'To a mind that is still the whole universe surrenders.'
    },{
      author: 'Chang Tzu',
      message: 'To a mind that is still the whole universe surrenders.'
    },{
      author: 'Robert James Waller',
      message: 'Life is never easy for those who dream.'
    },{
      author: 'Eleanor Roosevelt',
      message: 'The future belongs to those who believe in the beauty of their dreams.'
    },{
      author: 'Earl Nightingale',
      message: 'Our attitude toward life determines life’s attitude towards us.'
    },{
      author: 'Ralph Waldo Emerson',
      message: 'Do not go where the path may lead. Go instead where there is no path and leave a trail.'
    },{
      author: 'Confucius',
      message: 'Real knowledge is to know the extent of one’s ignorance.'
    }];

    // Get an eventual error defined in the URL query string:
    $scope.error = $location.search().err;

    // If user is signed in then redirect back home
    if ($scope.authentication.user) {
      //$location.path('/');
    }

    $scope.signup = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');

        return false;
      }

      $http.post('/api/auth/signup', $scope.credentials)
        .success(function (response) {

        //$scope.newUser = response;
          ToastService.show('Account was successfully created \'' + response.username + '\' with email \'' + response.email + '\'');

          $state.go($state.previous.state.name || 'admin.users', $state.previous.params);
        }).error(function (response) {

          $scope.error = response.message;
          ToastService.show('Err: ' + response.message);

        }).finally(function() {
          /**/
        });

    };

    $scope.signin = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');

        return false;
      }

      $http.post('/api/auth/signin', $scope.credentials).success(function (response) {
        // If successful we assign the response to the global user model
        $scope.authentication.user = response;
        Authentication.user = response;

        // And redirect to the previous or home page
        //$window.location.href = '/';

        var randomQuote = quotes[Math.floor(Math.random() * quotes.length)];

        ToastService.show('\"' + randomQuote.message + '\" - ' + randomQuote.author,10000);

        menu.populate();
        $state.go($state.previous.state.name || 'home', $state.previous.params);
      }).error(function (response) {
        $scope.error = response.message;
        ToastService.show('Err: ' + response.message);
      });
    };

    // OAuth provider request
    $scope.callOauthProvider = function (url) {
      if ($state.previous && $state.previous.href) {
        url += '?redirect_to=' + encodeURIComponent($state.previous.href);
      }

      // Effectively call OAuth authentication route:
      $window.location.href = url;
    };
  }
]);
