(function(){

  'use strict';

  angular.module('core')
    .factory('menu', [
      '$location', '$rootScope', '$mdSidenav', 'Authentication',
      function ($location, $rootScope, $mdSidenav, Authentication) {

        var vm = this;
        vm.authentication = Authentication;

        var sections = [];

        var homeSection = {
          name: 'Home',
          state: 'home',
          type: 'link'
        };

        var deskSection = {
          name: 'Desk',
          type: 'toggle',
          pages: [
            {
              name: 'Timesheet',
              type: 'link',
              state: 'desk.timesheet'
            }
          ]
        };

        var requestsSection = {
          name: 'Requests',
          type: 'toggle',
          pages: [
            {
              name: 'Holidays',
              type: 'link',
              state: 'requests.holidays'
            },
            {
              name: 'General',
              state: 'requests.general',
              type: 'link'
            },
            {
              name: 'Travel',
              state: 'requests.travel',
              type: 'link'
            },
            {
              name: 'Medical',
              state: 'requests.medical',
              type: 'link'
            }
          ]
        };

        var informationSection = {
          name: 'Information',
          type: 'toggle',
          pages: [
            {
              name: 'Conduct Code',
              type: 'link',
              state: 'information.conduct'
            },
            {
              name: 'Procedures',
              state: 'information.procedures',
              type: 'link'
            },
            {
              name: 'About CBS',
              state: 'information.about',
              type: 'link'
            },
            {
              name: 'Blog',
              state: 'information.blog',
              type: 'link'
            },
            {
              name: 'Events',
              state: 'information.events',
              type: 'link'
            }
          ]
        };

        var adminSection = {
          name: 'Admin',
          type: 'toggle',
          pages: [
            {
              name: 'Users',
              type: 'link',
              state: 'admin.users'
            },{
              name: 'Mail Chimp',
              type: 'link',
              state: 'admin.mailChimp'
            },
            {
              name: 'Statistics',
              state: 'admin.statistics',
              type: 'link'
            },
            {
              name: 'Notices',
              state: 'admin.notices',
              type: 'link'
            },
            {
              name: 'Queue',
              state: 'admin.queue',
              type: 'link'
            },
            {
              name: 'Clients and Projects',
              state: 'admin.clientProjects',
              type: 'link'
            }
          ]
        };

        if(vm.authentication.user) {

          sections.push(homeSection);
          sections.push(deskSection);
          sections.push(requestsSection);
          sections.push(informationSection);

          if (vm.authentication.user.roles.indexOf('admin') > -1) {
            sections.push(adminSection);
          }

        }

        var self= {

          sections: sections,
          homeSection: homeSection,
          deskSection: deskSection,
          adminSection: adminSection,
          informationSection: informationSection,
          requestsSection: requestsSection,

          toggleSelectSection: function (section) {
            self.openedSection = (self.openedSection === section ? null : section);
          },

          isSectionSelected: function (section) {
            return self.openedSection === section;
          },

          selectPage: function (section, page) {

            page && page.url && $location.path(page.url); // jshint ignore:line
            self.currentSection = section;
            self.currentPage = page;

          },

          populate: function(){

            if(vm.authentication.user) {

              sections.push(homeSection);
              sections.push(deskSection);
              sections.push(requestsSection);
              sections.push(informationSection);

              if (vm.authentication.user.roles.indexOf('admin') > -1) {
                sections.push(adminSection);
              }

            }

          }

        };

        return self;
      }]);

})();