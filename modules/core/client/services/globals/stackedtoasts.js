(function () {
  'use strict';

  angular
    .module('desk')
    .service('ToastService', ToastService);

  ToastService.$inject = ['$mdToast', '$timeout'];

  function ToastService($mdToast, $timeout) {

    var stack = [];
    var debounce;
    var lastMsg;
    var cooldown = 750;
    var defaultDuration = 3000;

    var toastPosition = angular.extend({},{
      bottom: true,
      top: false,
      left: false,
      right: true
    });

    function getToastPosition() {
      return Object.keys(toastPosition).filter(function(pos) { return toastPosition[pos]; }).join(' ');
    }

    var service = {};
    service.show = show;
    service.showWithAction = showWithAction;

    return service;

    function show(message, duration) {

      internalShow(
        null,
        null,

        $mdToast.simple()
        .textContent(message)
        .position(getToastPosition())
        .hideDelay(duration === undefined ? defaultDuration : duration)

      );

    }

    function showWithAction(id, message, actionText, responseFunction, duration) {

      internalShow(
        id,
        responseFunction,

        $mdToast.simple()
        .textContent(message)
        .action(actionText)
        .highlightAction(true)
        .highlightClass('md-accent')
        .position(getToastPosition())
        .hideDelay(duration === undefined ? defaultDuration : duration)

      );
    }

    function internalShow(id, responseFunction, toast) {

      (function () {

        if (lastMsg === toast._options.textContent)
          $timeout.cancel(debounce);

        lastMsg = toast._options.textContent;

        debounce = $timeout(function () {
          lastMsg = null;

          stack.push({
            id: id,
            toast: toast,
            responseFunction: responseFunction
          });

          if (stack.length === 1)
            showStacked();

        }, cooldown);

      })();

    }

    function showStacked() {
      if (!stack.length)
        return;

      var toastObject = stack[0];

      $mdToast.show(toastObject.toast)
        .then(function (response) {

          if (response === 'ok' && toastObject.responseFunction !== null) {
            toastObject.responseFunction(toastObject.id);
          }

          stack.shift();
          showStacked();

        });

    }

  }
})();