(function () {
  'use strict';

  angular.module('core').factory('GlobalService', ['$location', '$anchorScroll', function($location, $anchorScroll) {

    Date.prototype.sameDay = function(d) {
      return this.getFullYear() === d.getFullYear() && this.getDate() === d.getDate() && this.getMonth() === d.getMonth();
    };

    Date.prototype.getWeekNumber = function(){

      var d = new Date(+this);
      d.setHours(0,0,0,0);
      d.setDate(d.getDate()+4-(d.getDay()||7));

      return Math.ceil((((d-new Date(d.getFullYear(),0,1))/8.64e7)+1)/7);

    };

    return {

      weekDays: ['Sunday', 'Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],

      currentDate: {

        day: function(){
          return new Date().getDay();
        },

        month: function(){
          return ((new Date().getMonth()) + 1);
        },

        year: function(){
          return new Date().getFullYear();
        }

      },

      dateDiffInDays: function(a,b) {

        a = new Date(a);
        b = new Date(b);

        // Discard the time and time-zone information.
        var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
        var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

        return Math.floor((utc2 - utc1) / (1000 * 60 * 60 * 24));

      },

      addDays: function(date, days){

        var result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;

      },

      convertToDate : function (date){
        var returnDate = new Date(date.getFullYear() + '-' + (date.getMonth() + 1)+ '-' + date.getDate());
        returnDate.setHours(0,0,0,0);

        return returnDate;
      },

      convertTimeToDateTime : function (timeString) {

        var res = timeString.split(':');
        return new Date(2016, 10, 7, res[0], res[1], res[2]);
      },

      anchorScroll : function (location) {

        if ($location.hash() !== location)
          $location.hash(location);
         else
          $anchorScroll();

      },

      arraysEqual : function (arr1, arr2) {

        if(arr1.length !== arr2.length)
          return false;

        for(var i = arr1.length; i--;) {
          if(arr1[i] !== arr2[i])
            return false;
        }

        return true;
      },

      date_sort_asc : function (date1, date2) {

        if (date1 > date2) return 1;

        if (date1 < date2) return -1;

        return 0;
      },

      getIndex: function(obj,array){

        for(var i = 0; i < array.length; i++){
          if(obj === array[i]){
            return i;
          }
        }

        return -1;
      }

    };

  }]);

})();
