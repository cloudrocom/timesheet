/*
(function () {
  'use strict';

  angular.module('users').factory('ToastService', ['$mdToast', function($mdToast) {

    var toastPosition = angular.extend({},{
      bottom: true,
      top: false,
      left: false,
      right: true
    });

    function getToastPosition() {
      return Object.keys(toastPosition).filter(function(pos) { return toastPosition[pos]; }).join(' ');
    }

    return {

      showToastMessage: function(message, duration) {

        $mdToast.show(
          $mdToast.simple()
            .textContent(message)
            .position(getToastPosition())
            .hideDelay(duration)
        );

      },

      showToastMessageWithAction: function($scope, message, actionText, responseFunction){

        $mdToast.show(
          $mdToast.simple()
          .content(message)
          .action(actionText)
          .highlightAction(true)
          .highlightClass('md-accent')
          .position(getToastPosition())
        ).then(responseFunction);

        $scope.closeToast = function() {
          $mdToast.hide();
        };

      }

    };

  }]);

})();
*/
