(function(){

  'use strict';

  angular.module('core')
    .factory('DialogueService', [
      '$mdDialog',
      function ($mdDialog) {

        var object = {

          confirmDialogue: function(title, textContent, ariaLabel, targetEvent,
                                    okText, cancelText, okFunction, cancelFunction) {

            var confirm = $mdDialog.confirm()
              .clickOutsideToClose(true)
              .title(title)
              .textContent(textContent)
              .ariaLabel(ariaLabel)
              .targetEvent(targetEvent)
              .openFrom('#to-the-left')
              .closeTo('#to-the-right')
              .ok(okText)
              .cancel(cancelText);

            $mdDialog.show(confirm).then(okFunction, cancelFunction);

          }
        };

        return object;
      }]);

})();
