'use strict';

angular.module('core').controller('HomeController', ['$scope', 'Authentication', '$mdDialog', '$location', '$rootScope',
  function ($scope, Authentication, $mdDialog, $location, $rootScope) {

    var vm = this;
    vm.user = Authentication.user;

  }
]);
