'use strict';

angular.module('core').controller('NotFoundController',
  ['$scope', '$state', 'Authentication', 'NotificationService', '$mdDialog', '$mdSidenav', '$timeout', '$window', '$interval',

    function ($scope, $state, Authentication, NotificationService, $mdDialog, $mdSidenav, $timeout, $window, $interval) {

      var interval = null;
      $scope.countdown=10;

      var decrementCountdown = function(){

        $scope.countdown -=1;

        if($scope.countdown < 1){

          $state.go('home');
          $interval.cancel(interval);

        }

      };

      $scope.startCountDown = function(){
        interval = $interval(decrementCountdown,1000,$scope.countdown);
      };

    }

  ]);
