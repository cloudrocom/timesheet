'use strict';

angular.module('core').controller('ForbiddenController',['$scope', '$state',

    function ($scope, $state) {

      $state.go('authentication.signin');

    }

  ]);
