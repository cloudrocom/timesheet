/*jshint loopfunc: true */
'use strict';

angular.module('core').controller('SidenavController',
  ['$scope', '$state', 'Authentication', 'menu', 'NotificationService', '$mdDialog', '$mdSidenav', '$timeout',

    function ($scope, $state, Authentication, menu, NotificationService, $mdDialog, $mdSidenav, $timeout) {

      var vm = this;
      vm.user = Authentication.user;

      vm.isOpen = isOpen;
      vm.toggleOpen = toggleOpen;
      vm.autoFocusContent = false;
      vm.menu = menu;

      vm.status = {
        isFirstOpen: true,
        isFirstDisabled: false
      };

      function isOpen(section) {
        return menu.isSectionSelected(section);
      }

      function toggleOpen(section) {
        menu.toggleSelectSection(section);
      }

      vm.close = function () {
        $mdSidenav('left').close()
          .then(function () {
            //console.log('close LEFT is done');
          });
      };

    }
  ]);
