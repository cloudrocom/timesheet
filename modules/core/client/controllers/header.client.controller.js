/*jshint loopfunc: true */
'use strict';

angular.module('core').controller('HeaderController',
  ['$scope', '$state', 'Authentication', 'NotificationService', '$mdDialog', '$mdSidenav', '$timeout', '$window',

    function ($scope, $state, Authentication, NotificationService, $mdDialog, $mdSidenav, $timeout, $window) {

      var vm = this;
      var originatorEv;

      vm.authentication = Authentication;

      vm.notificationsArray = [];
      vm.activeNotifications = [];


      if(!vm.authentication.user && $window.location.pathname !== '/authentication/signin'){
        $state.go('authentication.signin');
      }

      vm.fetchNotices = function(){
        
        if(!vm.authentication.user) return;

        NotificationService.api.query({
          params:{
            userId: vm.authentication.user.id
          }
        }).$promise.then(function (notifications) {

          vm.notificationsArray = JSON.parse(JSON.stringify(notifications));

          for(var i = 0; i < vm.notificationsArray.length; i++){

            if(vm.notificationsArray[i].seen === false){

/*
                var message = vm.notificationsArray[i].message;
                var config = {
                  onclose: function(callbackNotification) {

                    for(var x = 0; x < vm.notificationsArray.length; x++){
                      if(vm.notificationsArray[x].title === callbackNotification.title){

                        vm.notificationsArray[x].seen = true;

                        NotificationService.update({
                          id: vm.notificationsArray[x].id
                        },vm.notificationsArray[x]).$promise.then(function (notification) {
                            //console.log(notification); // updated one comes back
                            vm.notificationsArray[x] = notification;
                          });
                      }
                    }

                  },
                  onopen: function() {
                    //console.log('The message is open!'  + vm.notificationsArray[i].title);
                  },
                  title: vm.notificationsArray[i].title
                };

                switch (vm.notificationsArray[i].type) {
                  case 'success':
                    growl.success(message, config);
                    break;
                  case 'info':
                    growl.info(message, config);
                    break;
                  case 'warning':
                    growl.warning(message, config);
                    break;
                  default:
                    growl.error(message, config);
                }
*/

            }

          }
        });
      };

      vm.openMenu = function($mdOpenMenu, ev) {
        originatorEv = ev;
        $mdOpenMenu(ev);
      };

      vm.menuItemClick = function() {
        originatorEv = null;
      };

      vm.toggleLeft = buildDelayedToggler('left');
      vm.isOpenLeft = function(){
        return $mdSidenav('left').isOpen();
      };

      function debounce(func, wait, context) {
        var timer;

        return function debounced() {
          var context = $scope,
            args = Array.prototype.slice.call(arguments);
          $timeout.cancel(timer);
          timer = $timeout(function() {
            timer = undefined;
            func.apply(context, args);
          }, wait || 10);
        };
      }

      function buildDelayedToggler(navID) {
        return debounce(function() {
          // Component lookup should always be available since we are not using `ng-if`
          $mdSidenav(navID)
            .toggle()
            .then(function () {
              //console.log('toggle ' + navID + ' is done');
            });
        }, 200);
      }

    }

  ]);
