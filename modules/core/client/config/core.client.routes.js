'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {

    // Redirect to 404 when route not found
    $urlRouterProvider.otherwise(function ($injector, $location) {
      $injector.get('$state').transitionTo('not-found', null, {
        location: false
      });
    });

    // Home state routing
    $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'modules/core/client/views/home.client.view.html',
      resolve: {
        app: function ($q, Authentication, $location) {

          var defer = $q.defer();

          if(Authentication.user){
            defer.resolve();
          }
          else{
            $location.path('/authentication/signin');
          }

          return defer.promise;
        }
      }
    })
    .state('not-found', {
      url: '/not-found',
      controller: 'NotFoundController',
      controllerAs: 'vm',
      templateUrl: 'modules/core/client/views/errors/404.client.view.html',
      data: {
        ignoreState: true
      }
    })
    .state('bad-request', {
      url: '/bad-request',
      templateUrl: 'modules/core/client/views/errors/400.client.view.html',
      data: {
        ignoreState: true
      }
    })
    .state('forbidden', {
      url: '/forbidden',
      controller: 'ForbiddenController',
      controllerAs: 'vm',
      templateUrl: 'modules/core/client/views/errors/403.client.view.html',
      data: {
        ignoreState: true
      }
    });
  }
]);
