'use strict';

module.exports = function (sequelize, DataTypes) {

  var Holidays = sequelize.define('Holidays', {

    fromDate: DataTypes.DATEONLY,
    toDate: DataTypes.DATEONLY,
    workDays: DataTypes.INTEGER,
    description: DataTypes.TEXT,
    accepted: DataTypes.BOOLEAN,
    requiresEdit: DataTypes.BOOLEAN,
    standInUser: DataTypes.INTEGER,
    standInType: DataTypes.TEXT,
    standInPerson: DataTypes.JSONB,
    destination: DataTypes.TEXT

  }, {
    classMethods: {
      associate: function (models) {

        Holidays.belongsTo(models.User, {

          foreignKey: 'UserId',
          foreignKeyConstraint: true

        });

      }
    }
  });

  return Holidays;
};
