'use strict';

module.exports = function (sequelize, DataTypes) {

  var Travel = sequelize.define('Travel', {

    fromDate: DataTypes.DATEONLY,
    toDate: DataTypes.DATEONLY,
    destination: DataTypes.TEXT,
    description: DataTypes.TEXT

  }, {
    classMethods: {
      associate: function (models) {

        Travel.belongsTo(models.User, {
          foreignKey: 'UserId',
          foreignKeyConstraint: true
        });

      }
    }
  });

  return Travel;
};
