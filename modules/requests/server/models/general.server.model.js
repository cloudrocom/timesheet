'use strict';

module.exports = function (sequelize, DataTypes) {

  var General = sequelize.define('General', {

    description: DataTypes.TEXT,
    requestType: DataTypes.TEXT

  }, {
    classMethods: {
      associate: function (models) {

        General.belongsTo(models.User, {
          foreignKey: 'UserId',
          foreignKeyConstraint: true
        });

      }
    }
  });

  return General;
};
