'use strict';

module.exports = function (sequelize, DataTypes) {

  var Medical = sequelize.define('Medical', {

    fromDate: DataTypes.DATEONLY,
    toDate: DataTypes.DATEONLY,
    description: DataTypes.TEXT,
    papers: DataTypes.ARRAY(DataTypes.TEXT)

  }, {
    classMethods: {
      associate: function (models) {

        Medical.belongsTo(models.User, {
          foreignKey: 'UserId',
          foreignKeyConstraint: true
        });

      }
    }
  });

  return Medical;
};
