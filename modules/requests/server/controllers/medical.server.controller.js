'use strict';

var path = require('path'),
  db = require(path.resolve('./config/lib/sequelize')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

exports.create = function (req, res) {

  db.Medical.create({

    fromDate: req.body.fromDate,
    toDate: req.body.toDate,
    description: req.body.description,
    papers: req.body.papers

  })
    .then(function (newMedical) {
      return res.json(newMedical);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });
};

exports.read = function (req, res) {

  db.Medical.find({
    where: {
      id: req.params.id
    }
  })
    .then(function (medical) {
      return res.json(medical);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.update = function (req, res) {

  db.Medical
    .findOne({
      where: {
        id: req.params.id
      }
    })
    .then(function (medical) {

      medical.updateAttributes({

        fromDate: req.body.fromDate,
        toDate: req.body.toDate,
        description: req.body.description,
        papers: req.body.papers

      })
        .then(function () {
          return res.json(medical);
        })
        .catch(function (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });

      return null;
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.delete = function (req, res) {

  db.Medical
    .findOne({
      where: {
        id: req.params.id
      }
    })
    .then(function (medical) {

      medical.destroy()
        .then(function () {
          return res.json(medical);
        })
        .catch(function (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });

      return null;
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.list = function (req, res) {

  db.Medical.findAll({

  })
    .then(function (medicals) {
      return res.json(medicals);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};
