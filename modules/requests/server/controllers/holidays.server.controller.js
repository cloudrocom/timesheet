'use strict';

var path = require('path'),
  db = require(path.resolve('./config/lib/sequelize')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

function addDays(date, days) {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}

exports.create = function (req, res) {
//addDays(req.body.date,1)
  db.Holidays.create({
    UserId: req.body.userId,
    fromDate: addDays(req.body.fromDate, 1),
    toDate: addDays(req.body.toDate, 1),
    workDays: req.body.workDays,
    description: req.body.description,
    accepted: false,
    standInUser: req.body.standInUser,
    standInType: req.body.standInType,
    standInPerson: req.body.standInPerson,
    destination: req.body.destination
  })
      .then(function (newHoliday) {
        return res.json(newHoliday);
      })
      .catch(function (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });
};

exports.read = function (req, res) {

  db.Holidays.find({
    where: {
      id: req.params.id
    }
  })
      .then(function (holiday) {
        return res.json(holiday);
      })
      .catch(function (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

};

exports.FindAllByUserId = function (req, res) {

  db.Holidays.findAll({
    where: {
      UserId: parseInt(req.params.userId)
    }
  })
      .then(function (holidays) {

        return res.json(holidays);

      })
      .catch(function (err) {

        return res.status(400).send({

          message: errorHandler.getErrorMessage(err)

        });

      });

};

exports.FindAllByDate = function (req, res) {

  var month = parseInt(req.params.month);

  var startMonth = '';
  var endMonth = '';

  var startDay = '';
  var endDay = '';

  if(month === 1){
    startMonth = '1';
    endMonth = '2';
    startDay = '1';
    endDay = '31';
  }
  else if(month === 12){

    startMonth = '11';
    endMonth = '12';
    startDay = '1';
    endDay = '31';
  }
  else{
    startMonth = (month - 1).toString();
    endMonth = (month + 1).toString();
    startDay = '20';
    endDay = '10';
  }

  startMonth = startMonth.length === 1 ? '0' + startMonth : startMonth;
  endMonth = endMonth.length === 1 ? '0' + endMonth : endMonth;

  db.Holidays.findAll({
    where: {
      UserId: parseInt(req.params.userId),
      $and: [
        {
          fromDate: {
            gte: new Date(req.params.year + '-' + startMonth.toString() + '-' + startDay) // 11
          }
        },
        {
          fromDate: {
            lte: new Date(req.params.year + '-' + endMonth.toString() + '-' + endDay) // 1
          }
        }
      ]
    }
  })
      .then(function (holidays) {
        return res.json(holidays);
      })
      .catch(function (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });
};

exports.update = function (req, res) {

  db.Holidays
      .findOne({
        where: {
          id: req.params.id
        }
      })
      .then(function (holiday) {
        holiday.updateAttributes({
          fromDate: addDays(req.body.fromDate, 1),
          toDate: addDays(req.body.toDate, 1),
          description: req.body.description,
          workDays: req.body.workDays,
          accepted: false,
          updatedAt: 'NOW()',
          standInUser: req.body.standInUser,
          standInType: req.body.standInType,
          standInPerson: req.body.standInPerson,
          destination: req.body.destination
        })
            .then(function () {
              return res.json(holiday);
            })
            .catch(function (err) {
              return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
              });
            });

        return null;
      })
      .catch(function (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

};

exports.delete = function (req, res) {

  db.Holidays
      .findOne({
        where: {
          id: req.params.id
        }
      })
      .then(function (holiday) {
        holiday.destroy()
            .then(function () {
              return res.json(holiday);
            })
            .catch(function (err) {
              return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
              });
            });

        return null;
      })
      .catch(function (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

};

exports.list = function (req, res) {

  db.Holidays.findAll({

  })
      .then(function (holidays) {
        return res.json(holidays);
      })
      .catch(function (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

};

exports.queue = function (req, res) {

  var userId = req.params.userId;
  var limit = req.params.limit;
  var offset = req.params.offset;

  var query = userId === 'all' ? {
    accepted: false
  } : {
    UserId: parseInt(userId),
    accepted: false
  };

  db.Holidays.findAndCountAll({
    where: query,
    include: [{
      all: true
    }],
    limit: limit,
    offset: offset,
    order: [
      ['createdAt', 'DESC']
    ]
  })
      .then(function (holidays) {
        return res.json(holidays);
      })
      .catch(function (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      });

};
