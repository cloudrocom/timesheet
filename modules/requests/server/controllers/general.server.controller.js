'use strict';

var path = require('path'),
  db = require(path.resolve('./config/lib/sequelize')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

exports.create = function (req, res) {

  db.General.create({

    description: req.body.description,
    UserId: req.body.userId,
    requestType: req.body.requestType

  })
    .then(function (newGeneral) {
      return res.json(newGeneral);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });
};

exports.read = function (req, res) {

  db.General.find({
    where: {
      id: req.params.id
    }
  })
    .then(function (general) {
      return res.json(general);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.update = function (req, res) {

  db.General
    .findOne({
      where: {
        id: req.params.id
      }
    })
    .then(function (general) {

      general.updateAttributes({

        description: req.body.description,
        updatedAt: req.body.updatedAt,
        requestType: req.body.requestType

      })
        .then(function () {
          return res.json(general);
        })
        .catch(function (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });

      return null;
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.delete = function (req, res) {

  db.General
    .findOne({
      where: {
        id: req.params.id
      }
    })
    .then(function (general) {

      general.destroy()
        .then(function () {
          return res.json(general);
        })
        .catch(function (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });

      return null;
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.list = function (req, res) {
  db.General.findAll({

  })
    .then(function (generals) {
      return res.json(generals);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};
