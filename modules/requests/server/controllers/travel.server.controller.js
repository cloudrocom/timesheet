'use strict';

var path = require('path'),
  db = require(path.resolve('./config/lib/sequelize')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

exports.create = function (req, res) {

  db.Travel.create({

    fromDate: req.body.fromDate,
    toDate: req.body.toDate,
    description: req.body.description,
    destination: req.body.destination

  })
    .then(function (newTravel) {
      return res.json(newTravel);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });
};

exports.read = function (req, res) {

  db.Travel.find({
    where: {
      id: req.params.id
    }
  })
    .then(function (travel) {
      return res.json(travel);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.update = function (req, res) {

  db.Travel
    .findOne({
      where: {
        id: req.params.id
      }
    })
    .then(function (travel) {

      travel.updateAttributes({

        fromDate: req.body.fromDate,
        toDate: req.body.toDate,
        description: req.body.description,
        destination: req.body.destination

      })
        .then(function () {
          return res.json(travel);
        })
        .catch(function (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });

      return null;
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.delete = function (req, res) {

  db.Travel
    .findOne({
      where: {
        id: req.params.id
      }
    })
    .then(function (travel) {

      travel.destroy()
        .then(function () {
          return res.json(travel);
        })
        .catch(function (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        });

      return null;
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};

exports.list = function (req, res) {

  db.Travel.findAll({

  })
    .then(function (travels) {
      return res.json(travels);
    })
    .catch(function (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });

};
