'use strict';

/**
 * Module dependencies
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Articles Permissions
 */
exports.invokeRolesPolicies = function () {
  acl.allow([
    {
      roles: ['admin'],
      allows: [
        {
          resources: '/api/holidays',
          permissions: '*'
        },
        {
          resources: '/api/holidays/:id',
          permissions: '*'
        },
        {
          resources: '/api/holidays/FindAllByUserId/:userId',
          permissions: '*'
        },
        {
          resources: '/api/holidays/FindAllByDate/:userId/:year/:month',
          permissions: '*'
        },
        {
          resources: '/api/holidays/queue/:userId/:limit/:offset',
          permissions: '*'
        }
      ]
    },
    {
      roles: ['user'],
      allows: [
        {
          resources: '/api/holidays',
          permissions: ['get','post']
        },
        {
          resources: '/api/holidays/:id',
          permissions: ['get','post']
        },
        {
          resources: '/api/holidays/FindAllByUserId/:userId',
          permissions: ['get','post']
        },
        {
          resources: '/api/holidays/FindAllByDate/:userId/:year/:month',
          permissions: ['get','post']
        },
        {
          resources: '/api/holidays/queue/:userId/:limit/:offset',
          permissions: ['get','post']
        }
      ]
    }
  ]);
};

/**
 * Check If Articles Policy Allows
 */
exports.isAllowed = function (req, res, next) {

  var roles = (req.user) ? req.user.dataValues.roles : ['guest'];

  // Check for user roles
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {

    if (err) {
      // An authorization error occurred
      return res.status(500).send('Unexpected authorization error');
    } else {
      if (isAllowed) {
        // Access granted! Invoke next middleware
        return next();
      } else {
        return res.status(403).json({
          message: 'User is not authorized'
        });
      }
    }

  });

};