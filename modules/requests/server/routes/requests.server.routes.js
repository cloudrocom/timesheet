'use strict';

var holidaysPolicy = require('../policies/holidays.server.policy'),
  holidays = require('../controllers/holidays.server.controller');

var generalPolicy = require('../policies/general.server.policy'),
  general = require('../controllers/general.server.controller');

var travelPolicy = require('../policies/travel.server.policy'),
  travel = require('../controllers/travel.server.controller');

var medicalPolicy = require('../policies/medical.server.policy'),
  medical = require('../controllers/medical.server.controller');

module.exports = function(app) {

  /* --- HOLIDAYS --- */

  app.route('/api/holidays').all(holidaysPolicy.isAllowed)
    .get(holidays.list)
    .post(holidays.create);

  app.route('/api/holidays/:id').all(holidaysPolicy.isAllowed)
      .get(holidays.read)
      .put(holidays.update)
      .delete(holidays.delete);

  app.route('/api/holidays/FindAllByUserId/:userId').all(holidaysPolicy.isAllowed)
      .get(holidays.FindAllByUserId);

  app.route('/api/holidays/FindAllByDate/:userId/:year/:month').all(holidaysPolicy.isAllowed)
      .get(holidays.FindAllByDate);

  app.route('/api/holidays/queue/:userId/:limit/:offset').all(holidaysPolicy.isAllowed)
      .get(holidays.queue);

  /* --- GENERAL --- */

  app.route('/api/general').all(generalPolicy.isAllowed)
      .get(general.list)
      .post(general.create);

  app.route('/api/general/:id').all(generalPolicy.isAllowed)
      .get(general.read)
      .put(general.update)
      .delete(general.delete);

  /* --- TRAVEL --- */

  app.route('/api/travel').all(travelPolicy.isAllowed)
      .get(travel.list)
      .post(travel.create);

  app.route('/api/travel/:id').all(travelPolicy.isAllowed)
      .get(travel.read)
      .put(travel.update)
      .delete(travel.delete);

  /* --- MEDICAL --- */

  app.route('/api/medical').all(medicalPolicy.isAllowed)
      .get(medical.list)
      .post(medical.create);

  app.route('/api/medical/:id').all(medicalPolicy.isAllowed)
      .get(medical.read)
      .put(medical.update)
      .delete(medical.delete);


};
