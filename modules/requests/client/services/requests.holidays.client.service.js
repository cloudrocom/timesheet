(function () {
  'use strict';

  angular
      .module('requests.services')
      .factory('HolidaysService', HolidaysService);

  HolidaysService.$inject = ['$resource'];

  function HolidaysService($resource) {

    return $resource('api/holidays/:id', {
      id: '@id'
    }, {

      update: {
        method: 'PUT'
      },

      FindAllByUserId: {
        url: 'api/holidays/FindAllByUserId/:userId',
        method: 'GET',
        isArray: true
      },

      FindAllByDate: {
        url: 'api/holidays/FindAllByDate/:userId/:year/:month',
        method: 'GET',
        isArray: true
      },

      queue: {
        url: 'api/holidays/queue/:userId/:limit/:offset',
        method: 'GET',
        isArray: false
      }

    });

  }
})();