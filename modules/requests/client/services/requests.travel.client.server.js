(function () {
  'use strict';

  angular
    .module('requests.services')
    .factory('TravelService', TravelService);

  TravelService.$inject = ['$resource'];

  function TravelService($resource) {

    return $resource('api/travel/:id', {
      id: '@id'
    }, {

      update: {
        method: 'PUT'
      }

    });

  }
})();