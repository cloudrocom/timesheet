(function () {
  'use strict';

  angular
    .module('requests.services')
    .factory('MedicalService', MedicalService);

  MedicalService.$inject = ['$resource'];

  function MedicalService($resource) {

    return $resource('api/medical/:id', {
      id: '@id'
    }, {

      update: {
        method: 'PUT'
      }

    });

  }
})();