(function () {
  'use strict';

  angular
    .module('requests.services')
    .factory('GeneralService', GeneralService);

  GeneralService.$inject = ['$resource'];

  function GeneralService($resource) {

    return $resource('api/general/:id', {
      id: '@id'
    }, {

      update: {
        method: 'PUT'
      },

      create: {
        method: 'POST'
      }

    });

  }
})();