'use strict';

angular.module('requests').controller('RequestsGeneralController',
    ['$scope', 'GeneralService', 'Authentication', 'ToastService', 'screenSize', '$state',
      function ($scope, GeneralService, Authentication, ToastService, screenSize, $state) {

        $scope.auth = Authentication;

        //User = $scope.auth.user
        //UserId = $scope.auth.user.id

        $scope.requestType = null;
        $scope.description = null;

         /*************** RESIZE ************************ */

        function isMobile(){
          return screenSize.is('xs, sm');
        }

        $scope.btnFlexValue = isMobile() ? 95 : 65;

        /************* SUBMIT **************************/

        $scope.btnClicked = function(){

          if($scope.requestType && $scope.description) {

            createNewEntry($scope.description, $scope.requestType);

          }
          else
              ToastService.show('Please insert request type and description first!', 1000);

        };

        /**************** DB ************************/

        function createNewEntry(description, type){
// db
          GeneralService.create({ //create new entry in db

            description: description,
            requestType: type,
            userId: $scope.auth.user.id


          }).$promise.then(function (data){

            ToastService.show('Entry inserted!', 1000);

            var result = JSON.parse(JSON.stringify(data));

            $state.go('home');

          });

        }

      }

    ]);