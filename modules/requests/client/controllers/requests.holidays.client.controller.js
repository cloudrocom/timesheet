'use strict';
/*jshint loopfunc: true */
angular.module('requests').controller('RequestsHolidaysController',
  ['$scope', '$filter', '$http', '$q', 'Users', 'GlobalService',
    'HolidaysService', 'Authentication', 'MaterialCalendarData',
    'screenSize', 'Admin', '$mdConstant', 'ToastService', 'TimesheetsService',

  function ($scope, $filter, $http, $q, Users, GlobalService,
            HolidaysService, Authentication, MaterialCalendarData,
            screenSize, Admin, $mdConstant, ToastService, TimesheetsService) {

    $scope.authentication = Authentication;

    $scope.selectedDates = [];
    $scope.calendarFlexValue = screenSize.is('xs, sm') ? 100 : 65;
    $scope.rowOrColumnForMobile = screenSize.is('xs, sm') ? 'column' : 'row';

    $scope.createNewButtonFlexValue = screenSize.is('xs, sm') ? 15 : 5;
    $scope.contactChipsFlexValue = screenSize.is('xs, sm') ? 85 : 95;

    initialize();


    //request to free days
    // request to holidays
    // free days stays
    // holidays gets hashed or something

    $scope.create = false;
    $scope.description = '';
    $scope.destination = '';
    $scope.displaySelectedDates = [];
    var weekday= GlobalService.weekDays;

    $scope.editing = false;
    $scope.createNewContact = false;

    $scope.newContactName = null;
    $scope.newContactEmail = null;
    $scope.newContactPhone = null;

    $scope.selectedKey = null;
    $scope.holidaysObjects = null;

    $scope.setDirection = function (direction) {
      //Calendar specific configuration
      $scope.direction = direction;
      $scope.dayFormat = direction === 'vertical' ? 'EEEE, MMMM d' : 'd';
    };

    $scope.dayClick = function (date) {

      //console.log('day click');
      //console.log($scope.selectedDates);

      var exists = false;

      if ($scope.editing) {
        for (var i = 0; i < GlobalService.dateDiffInDays($scope.holidaysObjects[$scope.selectedKey].fromDate,
               $scope.holidaysObjects[$scope.selectedKey].toDate) + 1; i++) {

          if (GlobalService.addDays($scope.holidaysObjects[$scope.selectedKey].fromDate, i).sameDay(date)) {
            //console.log('a');
            return 0;

          }
        }
      }

      for (var key in $scope.holidaysObjects) {
        for (var x = 0; x < GlobalService.dateDiffInDays($scope.holidaysObjects[key].fromDate,
          $scope.holidaysObjects[key].toDate) + 1; x++) {

          if (GlobalService.addDays($scope.holidaysObjects[key].fromDate, x).sameDay(date)) {
            exists = true;
            //console.log('b');
            $scope.selectedKey = key;
          }

        }
      }

      if (exists) {
        //console.log('c');
        // EDIT CURR HL
        var objectToEdit = $scope.holidaysObjects[$scope.selectedKey];

        $scope.description = objectToEdit.description;
        $scope.destination = objectToEdit.destination;

        if(objectToEdit.standInType === 'new'){

          var personDetails = objectToEdit.standInPerson;

          $scope.newContactName = personDetails.name;
          $scope.newContactEmail = personDetails.email;
          $scope.newContactPhone = personDetails.phone;

          $scope.createNewContact = true;
          $scope.contacts = [];
          //console.log('d');
        }
        else if(objectToEdit.standInType === 'user'){

          $scope.newContactName = null;
          $scope.newContactEmail = null;
          $scope.newContactPhone = null;

          loadContact(objectToEdit.standInUser);
          $scope.createNewContact = false;
          //console.log('e');
        }

        $scope.selectedDates = []; //console.log('clear 95');

        //console.log(GlobalService.dateDiffInDays($scope.holidaysObjects[$scope.selectedKey].fromDate, $scope.holidaysObjects[$scope.selectedKey].toDate) + 1);

        for (var y = 0; y < GlobalService.dateDiffInDays($scope.holidaysObjects[$scope.selectedKey].fromDate,
          $scope.holidaysObjects[$scope.selectedKey].toDate) + 1; y++) {

          //console.log('< --- > ');
          //console.log(GlobalService.addDays($scope.holidaysObjects[$scope.selectedKey].fromDate, y));
          //console.log('< --- > ');

          $scope.selectedDates.push(GlobalService.convertToDate(GlobalService.addDays($scope.holidaysObjects[$scope.selectedKey].fromDate, y)));
        }

        //console.log($scope.selectedDates);

        $scope.editing = true;
        $scope.create = false;
      }
      else {
        // CREATE NEW HL
        //console.log('f');
        $scope.create = true;

        if ($scope.editing) {

          emptyFields();

          $scope.selectedDates = [date];
          $scope.selectedKey = null;
          $scope.create = true;
        }

      }

      $scope.displaySelectedDates = [];
      for(var dateIndex in $scope.selectedDates){

        var datey = new Date($scope.selectedDates[dateIndex]);

        $scope.displaySelectedDates.push({
          dayName: weekday[datey.getDay()],
          dayNumber: datey.getDate(),
          month: datey.getMonth() + 1,
          year: datey.getFullYear()
        });

      }

    };

    $scope.prevMonth = function (date) {
      // ideas ?
      $scope.editing = false;
      $scope.create = false;
      $scope.selectedDates = [];
      $scope.displaySelectedDates = [];

      getHolidaysData(date.year, date.month);
    };

    $scope.nextMonth = function (date) {
      // ideas ?
      $scope.editing = false;
      $scope.create = false;
      $scope.selectedDates = [];
      $scope.displaySelectedDates = [];

      getHolidaysData(date.year, date.month);
    };

    $scope.newContact = function(){
      $scope.createNewContact = true;
    };

    $scope.backToUsers = function(){
      $scope.createNewContact = false;
    };

    function getRequestArray() {

      if($scope.selectedDates.length === 1){

        return [{
          fromDate: $scope.selectedDates[0],
          toDate: $scope.selectedDates[0]
        }];

      }

      var requestArray = [];
      var startFrom = null;
      var indexer = 1;

      for (var y in $scope.selectedDates) {

        if (startFrom === null) {
          startFrom = $scope.selectedDates[y];
          continue;
        }

        if (GlobalService.addDays(startFrom, indexer).sameDay($scope.selectedDates[y])) {
          indexer++;
        }
        else {

          requestArray.push({
            fromDate: startFrom,
            toDate: $scope.selectedDates[y - 1]
          });

          startFrom = $scope.selectedDates[y];
          indexer = 1;
        }

        if ($scope.selectedDates[y].sameDay($scope.selectedDates[$scope.selectedDates.length - 1])) {

          requestArray.push({
            fromDate: startFrom,
            toDate: $scope.selectedDates[$scope.selectedDates.length - 1]
          });

        }

      }

      return requestArray;
    }

    function calculateWorkDays(obj) {

      var workdays = 0;

      for (var i = 0; i < GlobalService.dateDiffInDays(obj.fromDate, obj.toDate) + 1; i++) {

        if (GlobalService.addDays(obj.fromDate, i).getDay() === 6 || GlobalService.addDays(obj.fromDate, i).getDay() === 0)
          continue;

        workdays++;
      }

      return workdays;
    }

    function emptyFields(){

      $scope.selectedDates = []; //console.log('clear 224');
      $scope.description = '';
      $scope.destination = '';
      $scope.create = false;
      $scope.editing = false;
      $scope.contacts = [];

      $scope.newContactName = null;
      $scope.newContactEmail = null;
      $scope.newContactPhone = null;

    }

    function getNewCreatedContact(){

      var newCreatedContact = {};

      newCreatedContact.name = $scope.newContactName;
      newCreatedContact.email = $scope.newContactEmail;
      newCreatedContact.phone = $scope.newContactPhone;

      $scope.newContactName = null;
      $scope.newContactEmail = null;
      $scope.newContactPhone = null;


      return newCreatedContact;
    }

    $scope.insertHolidayData = function () {

      $scope.selectedDates.sort(GlobalService.date_sort_asc);
      var requestArray = getRequestArray();

      var newCreatedContact = {};

      if($scope.createNewContact){
        newCreatedContact = getNewCreatedContact();
      }

      for (var x in requestArray) {

        var workdays = calculateWorkDays(requestArray[x]);

        $http.post('/api/holidays', {

          userId: $scope.authentication.user.id,
          fromDate: requestArray[x].fromDate,
          toDate: requestArray[x].toDate,
          workDays: workdays,
          description: $scope.description, //$scope.description
          standInUser: $scope.createNewContact ? 0 : $scope.contacts[0].id,
          standInType: $scope.createNewContact ? 'new' : 'user',
          standInPerson: $scope.createNewContact ? {
            name: newCreatedContact.name,
            email: newCreatedContact.email,
            phone: newCreatedContact.phone
          } : null,
          destination: $scope.destination

        }).success(function (response) {

          for (var i = 0; i < GlobalService.dateDiffInDays(response.fromDate, response.toDate) + 1; i++) {
            MaterialCalendarData.setDayContent(GlobalService.addDays(response.fromDate, i), response.description);
          }

          $scope.holidaysObjects.push(response);
          ToastService.show('Entry inserted!',1000);

        }).error(function (response) {
          ToastService.show('Entry not inserted ! ' + response.message,1000);
        });

      }
      emptyFields();
    };

    $scope.updateHolidayData = function () {

      //TODO if(accepted) // could not be edited

      var requestArray = getRequestArray();
      var workdays = calculateWorkDays(requestArray[0]);

      var newCreatedContact = {};

      if($scope.createNewContact){
        newCreatedContact = getNewCreatedContact();
      }

      $http.put('/api/holidays/' + $scope.holidaysObjects[$scope.selectedKey].id, {

        fromDate: requestArray[0].fromDate,
        toDate: requestArray[0].toDate,
        workDays: workdays,
        description: $scope.description, //$scope.description
        standInUser: $scope.createNewContact ? 0 : $scope.contacts[0].id,
        standInType: $scope.createNewContact ? 'new' : 'user',
        standInPerson: $scope.createNewContact ? {
          name: newCreatedContact.name,
          email: newCreatedContact.email,
          phone: newCreatedContact.phone
        } : null,
        destination: $scope.destination

      }).success(function (response) {

        ToastService.show('Entry inserted!',1000);

        for(var i in $scope.holidaysObjects){

          if($scope.holidaysObjects[i].id === response.id) {

            var fromDate = new Date(response.fromDate);
            fromDate.setDate(fromDate.getDate()-1);

            var toDate = new Date(response.toDate);
            toDate.setDate(toDate.getDate()-1);

            $scope.holidaysObjects[i] = response;
            $scope.holidaysObjects[i].fromDate = fromDate;
            $scope.holidaysObjects[i].toDate = toDate;

/*            $scope.holidaysObjects[i].workdays = response.workDays;
            $scope.holidaysObjects[i].description = response.description;
            $scope.holidaysObjects[i].standInUser = response.standInUser;
            $scope.holidaysObjects[i].standInType = response.standInType;
            $scope.holidaysObjects[i].standInPerson = response.standInPerson;*/

            for (var y = 0; y < GlobalService.dateDiffInDays(response.fromDate, response.toDate) + 1; y++) {
              MaterialCalendarData.setDayContent(GlobalService.addDays(fromDate, y), response.description);
            }

          }
        }

      }).error(function (response) {
        ToastService.show('Entry not inserted ! ' + response.message,1000);
      });

      emptyFields();
    };

    function initialize() {

      getHolidaysData(GlobalService.currentDate.year(), GlobalService.currentDate.month());
    }

    function getHolidaysData(year, month){

      HolidaysService.FindAllByDate({
        userId: $scope.authentication.user.id,
        year: year,
        month: month
      })
        .$promise.then(function (data) {

          // gets the data, takes it out into stringify, transforms it onto an javascript object.
          $scope.holidaysObjects = JSON.parse(JSON.stringify(data));

          //In the foreach, it iterates through each day, used to set <HTML> tags inside the day square
          //For each properties check Server->Models
          for (var key in $scope.holidaysObjects) {

            var currentObj = $scope.holidaysObjects[key];

            for (var i = 0; i < GlobalService.dateDiffInDays(currentObj.fromDate, currentObj.toDate) + 1; i++) {
              MaterialCalendarData.setDayContent(GlobalService.convertToDate(GlobalService.addDays(currentObj.fromDate, i)), currentObj.description);
            }

          }

        });

    }

    $scope.setDayContent = function (date) {
      return '<p></p>';
    };

    /* --- CONTACTS LOGIC --- */
    $scope.allContacts = loadContacts();

    $scope.querySearch = querySearch;
    $scope.keys = [$mdConstant.KEY_CODE.ENTER, $mdConstant.KEY_CODE.COMMA];

    $scope.contacts = [/*{

      id: $scope.authentication.user.id,
      name: $scope.authentication.user.displayName,
      image: $scope.authentication.user.profileImageURL

    }*/];

    function querySearch (criteria) {
      return $scope.allContacts.filter(createFilterFor(criteria));
    }

    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(contact) {
        return (contact._lowername.indexOf(lowercaseQuery) !== -1);
      };

    }

    function loadContacts() {

      Admin.listUsers({})
        .$promise.then(function (data) {

          var contacts = JSON.parse(JSON.stringify(data));

          var response = contacts.rows.map(function (user, index) {

            var contact = {
              id: user.id,
              name: user.displayName,
              image: user.profileImageURL
            };

            contact._lowername = contact.name.toLowerCase();
            return contact;

          });

          $scope.allContacts = response;
        });
    }

    function loadContact(userId){

      $scope.contacts = [];

      Admin.getUser({
        userId: userId
      })
        .$promise.then(function (data) {

          var userFromTheSelectedDay = JSON.parse(JSON.stringify(data));

          $scope.contacts.push({

            id: userFromTheSelectedDay.id,
            name: userFromTheSelectedDay.displayName,
            image: userFromTheSelectedDay.profileImageURL

          });

        });

    }

  }

]);

/* --- KNOWN BUG --- */
// https://github.com/angular/material/issues/9520 //
//  Cannot read property 'nextTick' of undefined  //