(function() {
  'use strict';

  angular
    .module('requests.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('requests', {
        abstract: true,
        url: '/requests',
        template: '<ui-view/>'
      })
      .state('requests.holidays', {
        url: '/holidays',
        templateUrl: 'modules/requests/client/views/requests.holidays.client.view.html',
        controller: 'RequestsHolidaysController',
        controllerAs: 'vm'
      })
      .state('requests.general', {
        url: '/general',
        templateUrl: 'modules/requests/client/views/requests.general.client.view.html',
        controller: 'RequestsGeneralController',
        controllerAs: 'vm'
      })
      .state('requests.travel', {
        url: '/travel',
        templateUrl: 'modules/requests/client/views/requests.travel.client.view.html',
        controller: 'RequestsTravelController',
        controllerAs: 'vm'
      })
      .state('requests.medical', {
        url: '/medical',
        templateUrl: 'modules/requests/client/views/requests.medical.client.view.html',
        controller: 'RequestsMedicalController',
        controllerAs: 'vm'
      });
  }
})();