(function (app) {
  'use strict';

  app.registerModule('requests');
  app.registerModule('requests.services');
  app.registerModule('requests.routes', ['ui.router', 'requests.services']);
})(ApplicationConfiguration);
